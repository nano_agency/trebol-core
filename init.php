<?php
/**
 * Plugin Name: Trebol Core Plugin
 * Plugin URI: http://www.nanoagency.co
 * Description: This is not just a plugin, it is a package with many tools a website needed.
 * Author: NanoAgency
 * Version: 1.0.1
 * Author URI: http://www.nanoagency.co
 * Text Domain: nano
*/

require_once('core-config.php');
require_once('inc/helpers/vc.php');

/*  Include Meta Box ================================================================================================ */
require_once( 'inc/meta-box/meta-box-master/meta-box.php');
require_once( 'inc/meta-box/meta-boxes.php');

/*  Include Post Format ============================================================================================= */
require_once( 'inc/vafpress-post-formats/vp-post-formats-ui.php');

/*  Importer ======================================================================================================== */
require_once( 'inc/importer/importer.php');

//Shortcode
require_once( 'inc/shortcode/na_blogs_featured.php');
require_once( 'inc/shortcode/na_blog.php');
require_once( 'inc/shortcode/na_heading_title.php');
require_once( 'inc/shortcode/na_masonry_category.php');
require_once( 'inc/shortcode/na_tabscategory.php');
require_once( 'inc/shortcode/na_category_colections.php');
require_once( 'inc/shortcode/na_category_slider.php');
require_once( 'inc/shortcode/na_category_banner.php');
require_once( 'inc/shortcode/na_products.php');
require_once( 'inc/shortcode/na_testimonial.php');
require_once( 'inc/shortcode/na_intro.php');
require_once( 'inc/shortcode/na_video.php');
require_once( 'inc/shortcode/na_tabsproduct.php');
require_once( 'inc/shortcode/na_countdown_banner.php');
require_once( 'inc/shortcode/na_feature_services.php');
require_once( 'inc/shortcode/na_brand.php');
require_once( 'inc/shortcode/na_banner_split.php');
require_once( 'inc/shortcode/na_slider_show_case.php');
require_once( 'inc/shortcode/na_team.php');

?>