<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$idran= random_int(0,99);
function get_TabTitle($type){
    switch ($type) {
        case 'latest':
            return array('title'=> esc_html__('New Arrivals','nano'));
        case 'featured':
            return array('title'=> esc_html__('Featured','nano'));
        case 'toprate':
            return array('title'=> esc_html__('Top Rated','nano'));
        case 'best-selling':
            return array('title'=> esc_html__('Best Seller','nano'));
        case 'onsale':
            return array('title'=> esc_html__('Onsale','nano'));
            break;
    }
}
//category
if(isset($atts['category']) && empty($atts['category'])){
    $args = array(
        'post_type'     => 'product',
        'post_status'   => 'publish',
        'tax_query'     => array(
            array(
                'taxonomy'  => 'product_cat'
            )
        )
    );
    $query_shop = new WP_Query($args);
}
else{
    $terms=explode(',', $atts['category']);
    $args= array(
        array(
            'taxonomy'          => 'product_cat',
            'field'             => 'slug',
            'terms'             => $terms,
            'posts_per_page'    => -1,
            'post_status'       => 'publish',
        )

    );
    $atts['tax_query']= $args;
}


//list tabs
$types = explode(',',$atts['list_type']);
foreach ($types as $type) {
    $list_types[$type] = get_TabTitle($type);
}

?>

<div class="block-product-tabs na-product-tabs na-block">
    <div class="na-block-content">
        <?php if ( $atts['box_title'] ) {?>
            <h3 class="widgettitle box-title tabs-title <?php echo $atts['title_align']; ?>">
                <?php echo htmlspecialchars_decode( $atts['box_title'] ); ?>
            </h3>
        <?php }?>
        <div data-example-id="togglable-tabs" role="tabpanel" class="box-content">    
            <ul role="tablist" class="nav nav-tabs nav-product-tabs list-unstyled list-inline <?php echo $atts['style']; ?> <?php echo $atts['title_align']; ?>">
                <?php
                    $count=0;
                    foreach ($list_types as $key =>$list) {
                    ?>
                        <li class="<?php echo ($count==0)?'active':''; ?>" role="presentation">
                            <a class="tabs-title-product" aria-expanded="true" data-toggle="tab" role="tab" href="#<?php echo  $key; ?><?php echo $idran; ?>">
                                <?php echo $list['title']; ?>
                            </a>
                        </li>
                    <?php
                    $count++;
                    }
                ?>
            </ul>    
            <div class="tab-content widgetcontent clearfix" id="tabs_product<?php echo $idran; ?>">
                <?php $count=0; ?>
                <?php foreach ($list_types as $key => $list) {
                    switch ($key) {
                        case 'best-selling':
                            $atts['meta_key'] = 'total_sales';
                            $atts['order']    = 'DESC';
                            $atts['orderby']  = 'meta_value_num';
                            $meta_query = WC()->query->get_meta_query();
                            $atts['tax_query']= $args;
                            $atts['post__in'] = '';
                            $atts['meta_query'] = $meta_query;
                            break;

                        case 'toprate':
                            $atts['meta_key'] = '_wc_average_rating';
                            $atts['orderby']  = 'meta_value_num';
                            $meta_query = WC()->query->get_meta_query();
                            $atts['tax_query']= $args;
                            $atts['post__in'] = '';
                            $atts['meta_query'] = $meta_query;
                            break;

                        case 'latest':
                            $meta_query = WC()->query->visibility_meta_query();
                            $meta_query = WC()->query->stock_status_meta_query();
                            $atts['meta_query'] = $meta_query;
                            break;

                        case 'featured':
                            $atts['meta_key'] = '';
                            $atts['orderby']  = '';
                            $atts['post__in'] = '';
                            $meta_query = array_merge( $atts['tax_query'], WC()->query->get_tax_query() );
                            $meta_query[]= array(
                                'taxonomy'         => 'product_visibility',
                                'terms'            => 'featured',
                                'field'            => 'name',
                                'operator'         => 'IN',
                                'include_children' => false,
                            );
                            $atts['tax_query'] = $meta_query;
                            break;

                        case 'onsale':
                            $product_ids_on_sale = wc_get_product_ids_on_sale();
                            $product_ids_on_sale[]  = 0;
                            $atts['meta_key'] = '';
                            $atts['orderby']  = '';
                            $atts['post__in'] = $product_ids_on_sale;
                            $atts['tax_query']= $args;
                            $meta_query = WC()->query->get_meta_query();
                            $atts['meta_query'] = $meta_query;
                            break;
                    }
                    ?>
                    <div id="<?php echo $key; ?><?php echo $idran; ?>" class="tab-pane <?php echo ( $count==0 )?' active in':''; ?> " role="tabpanel">
                        <?php
                            $output = nano_template_part('box-layouts/shortcode', 'product-grid', array('atts' => $atts));
                        ?>
                    </div>
                    <?php $count++;
                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>