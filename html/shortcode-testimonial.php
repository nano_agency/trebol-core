<?php

$image_size = array(120,120);
$css_class  = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items      = (array) vc_param_group_parse_atts($atts['items']);

?>

<div class="block-testimonial na-block <?php echo esc_attr( $atts['style'] ).' '.esc_attr( $css_class );?>">
    <div class="na-block-content"> 
        <?php if ( trim( $atts['title'] ) != '' ) { ?>
            <h2 class="widgettitle box-title">
                <span><?php echo esc_attr( $atts['title'] ); ?></span>                
            </h2>
        <?php } ?>
        <div class="box-testimonial-wrap box-content">            
            <?php foreach( $items as $item ): ?>
                <?php $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) ); ?>                
                <div class="box-testimonial">    
                    <?php if( $item['image_box'] ) { ?>
                        <div class="box-image">
                            <?php echo $img['thumbnail']; ?>
                        </div>
                    <?php } ?>
                    <div class="box-content-inner">
                        <?php if ( trim( $item['quotes'] ) != '' ) { ?>
                            <div class="box-description">                                    
                                <?php echo esc_html($item['quotes']); ?>
                            </div>
                        <?php } ?>
                        <div class="box-meta-title">
                            <?php if ( trim( $item['name'] ) != '' ) { ?>
                                <h3 class="box-title">                                    
                                    <?php echo esc_html($item['name']); ?>
                                </h3>
                            <?php } ?>

                            <?php if ( trim( $item['job_titles'] ) != '' ) { ?>
                                <ul class="box-meta list-unstyled">                                    
                                    <li class="box-meta-item"><?php echo esc_html($item['job_titles']); ?></li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>                    
            <?php endforeach; ?>
        </div>
    </div>
</div>

