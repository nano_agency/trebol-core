<?php

$css_class  = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$link       = trim( $atts['url'] );
$link       = ( '||' === $link ) ? '' : $link;
$link       = vc_build_link( $link );
$image_size = array(737,682);
$images     = explode(",",$atts['image_box']);

?>

<div class="block-category-slider na-block <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] );?>">  
    <div class="na-block-content"> 
        <?php if ( isset( $atts['name'] ) ) { ?>                        
            <p class="box-name"><?php echo trim( $atts['name'] ); ?></p>
        <?php } ?>                                               
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="category-slider-carousel">                                         
                    <?php if( $images) { ?>
                        <div class="slider-nav">
                            <button type="button" class="prev-slide"><i class="icon ion-android-arrow-back"></i><span><?php echo esc_html__( 'Previous', 'trebol' ); ?></span></button><button type="button" class="next-slide"><span><?php echo esc_html__( 'Next', 'trebol' ); ?></span><i class="icon ion-android-arrow-forward"></i></button>                            
                        </div>
                        <ul class="box-category-slider list-unstyled">                                                       
                            <?php foreach( $images as $image ){
                                $img = wpb_getImageBySize( array( 'attach_id' => (int) $image, 'thumb_size' => $image_size ) );      
                            ?>
                            <li class="box-image">                                    
                                <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                    <?php echo $img['thumbnail']; ?>
                                </a>
                            </li>                                
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <?php if ( isset( $atts['subtitle'] ) ) { ?>                        
                        <p class="box-subtitle"><?php echo trim( $atts['subtitle'] ); ?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="category-slider-content">
                    <?php if ( isset( $atts['title'] ) ) { ?>                        
                        <h3 class="box-title"><?php echo trim( $atts['title'] ); ?></h3>
                    <?php } ?>
                    <?php if ( strlen( $link['url'] ) > 0 ) { ?>
                        <div class="box-button-link">
                            <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                                <span><?php echo esc_html($link['title']);?></span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>        
    </div>
</div>