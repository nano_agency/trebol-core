<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$idran= random_int(0,99);

if ( ! function_exists( 'get_TabTitle' ) ) :

    function get_TabTitle($type){
        switch ($type) {
            case 'latest':
                return array('title'=> esc_html__('New Arrivals','nano'));
            case 'featured':
                return array('title'=> esc_html__('Featured','nano'));
            case 'toprate':
                return array('title'=> esc_html__('Top Rated','nano'));
            case 'best-selling':
                return array('title'=> esc_html__('Best Seller','nano'));
            case 'onsale':
                return array('title'=> esc_html__('Onsale','nano'));
            break;
        }
    }
        
endif;

//category
if(isset($atts['category']) && empty($atts['category'])){
    $args = array(
        'post_type'     => 'product',
        'post_status'   => 'publish',
        'tax_query'     => array(
            array(
                'taxonomy'  => 'product_cat'
            )
        )
    );
    $query_shop = new WP_Query($args);
}
else{
    $terms=explode(',', $atts['category']);
    $args= array(
        array(
            'taxonomy'          => 'product_cat',
            'field'             => 'slug',
            'terms'             => $terms,
            'posts_per_page'    => -1,
            'post_status'       => 'publish'
        )

    );
    $atts['tax_query']= $args;
}
$atts['posts_per_page']   = ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page');
$atts['paged']            = ((nano_get_query_var('paged')) ? nano_get_query_var('paged') : 1);

//list tabs
$types = explode(',',$atts['list_type']);
foreach ($types as $type) {
    $list_types[$type] = get_TabTitle($type);
}

$url        = vc_build_link( $atts['url'] );
$position   = $atts['position_layouts'];
?>
<div class="block-category-colections na-block layout-<?php echo esc_attr($position); ?>">            
    <div class="na-block-content" id="block-products<?php echo $idran; ?>">

        <?php switch ($atts['list_type']) {
            case 'best-selling':
                $atts['meta_key'] = 'total_sales';
                $atts['order']    = 'DESC';
                $atts['orderby']  = 'meta_value_num';
                $meta_query = WC()->query->get_meta_query();
                $atts['meta_query'] = $meta_query;
                break;

            case 'toprate':
                $atts['meta_key'] = '_wc_average_rating';
                $atts['orderby']  = 'meta_value_num';
                $meta_query = WC()->query->get_meta_query();
                $atts['meta_query'] = $meta_query;
                break;

            case 'featured':
                $atts['tax_query'] = array_merge( $atts['tax_query'], WC()->query->get_tax_query() );
                $atts['tax_query'][]= array(
                    'taxonomy'         => 'product_visibility',
                    'terms'            => 'featured',
                    'field'            => 'name',
                    'operator'         => 'IN',
                    'include_children' => false,
                );
                break;

            case 'onsale':
                $product_ids_on_sale = wc_get_product_ids_on_sale();
                $atts['post__in'] = $product_ids_on_sale;
                $meta_query = WC()->query->get_meta_query();
                $atts['meta_query'] = $meta_query;
                break;

            default:
                $meta_query = WC()->query->visibility_meta_query();
                $meta_query = WC()->query->stock_status_meta_query();
                $atts['meta_query'] = $meta_query;
                break;
        } ?>
        
        <div class="box-category-colections box-category-colection-image">
            <div class="box-content">

                <?php 
                    $img = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box'], 'thumb_size' => array(720,366) ) ); 
                ?>

                <?php if($atts['image_box']) { ?>
                    <div class="box-image">
                        <a href="<?php echo esc_url($url['url']);?>" class="box-image-link">
                            <?php echo $img['thumbnail']; ?>
                        </a>
                    </div>
                <?php } ?>

                <div class="box-image-content">
                    <?php if ( trim( $atts['title_box'] ) != '' ) { ?>
                        <h3 class="box-title">
                            <span><?php echo trim( $atts['title_box'] ); ?></span>
                        </h3>
                    <?php } ?>

                    <?php if ( trim( $url['url'] ) != '' ) { ?>
                        <div class="box-button-link">
                            <a class="btn btn-link-second" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                        </div>
                    <?php } ?>                    
                </div>
            </div>
        </div>

        <div class="box-category-colections box-category-colection-products">
            <?php $output = nano_template_part('box-layouts/shortcode', 'product-'.$atts['box_layouts'], array('atts' => $atts)); ?>        
        </div>        
    </div>
</div>



