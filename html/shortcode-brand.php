<?php

$image_size        = array(370,380);
$css_class         = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items             = (array) vc_param_group_parse_atts($atts['items']);

?>

<div class="block-brand na-block na-block-brand <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] );?>">  
    <div class="na-block-content box-brand">                 
        <?php foreach( $items as $item ): ?>                                
            <?php 
                $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) ); 
                $link = trim( $item['url'] );
                $link = ( '||' === $link ) ? '' : $link;
                $link = vc_build_link( $link );    
            ?>   
            <?php if( $item['image_box'] || strlen( $link['url'] ) > 0 ) { ?>
                <div class="box-brand-item">
                    <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link" title="<?php echo esc_attr($link['title']);?>">
                        <?php echo $img['thumbnail']; ?>
                    </a>
                </div>
            <?php } ?>            
        <?php endforeach; ?>         
    </div>
</div>
