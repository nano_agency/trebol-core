<?php

/**
* The default template for displaying content
*
* @author      NanoAgency
* @link        http://nanoagency.co
* @copyright   Copyright (c) 2018 NanoAgency
* @license     GPL v2
*/

$url              = vc_build_link( $atts['url'] );
$video_url        = vc_build_link( $atts['video_link'] );
$image_size       = array(585,420);
$image_size_large = array(870,620);
$image_size_full  = array(1170,520);
$css_class        = vc_shortcode_custom_css_class( $atts['css'], ' ' );

?>

<div class="block-video na-block na-block-video <?php echo esc_attr( $atts['el_class'] ).' '.esc_attr( $css_class ).' '.esc_attr( $atts['style'] ); ?>"> 
    <div class="na-block-content">
        <?php if ( $atts['style'] == 'style1' ): ?>
            <div class="box-video">
                <div class="box-video-intro <?php echo esc_attr( $atts['align'] ); ?>">
                    <div class="box-video-intro-wrap">
                        <?php if ( trim( $atts['title'] ) != '' ) { ?>
                            <h2 class="widgettitle box-title">
                                <span><?php echo trim( $atts['title'] ); ?></span>
                            </h2>
                        <?php } ?>

                        <?php if ( trim( $atts['description'] ) != '' ) { ?>
                            <div class="box-description">
                                <?php echo trim( $atts['description'] ); ?>
                            </div>
                        <?php } ?>

                        <?php if ( trim( $url['url'] ) != '' ) { ?>
                            <div class="box-button-link">
                                <a class="btn btn-link-primary" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="box-video-images">
                    <?php 
                        $img = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box'], 'thumb_size' => $image_size ) );     
                    ?>      
                    <?php if( $atts['image_box'] ) { ?>
                        <div class="box-image">
                            <?php echo $img['thumbnail']; ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if ( trim( $video_url['url'] ) != '' ) { ?>
                    <button type="button" class="btn btn-video" data-toggle="modal" data-target="#modal-video">
                        <span class="sr-only sr-only-focusable"><?php echo esc_html( $video_url['title'] );?></span>
                        <span aria-hidden="true" class="icon-control-play"></span>                    
                    </button>             
                    <div class="modal fade trebol_popup_modal" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="Title" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">                            
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span class="icon ion-android-close"></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <?php if($video_url['url'] ){?>
                                        <div class="embed-responsive-16by9  post-video single-video embed-responsive-16by9">
                                            <?php $sp_video = $video_url['url'] ; ?>
                                            <?php if(wp_oembed_get( $sp_video )) { ?>
                                                <?php  echo nano_oembed_get($sp_video, 0,'nano-video'); ?>
                                            <?php } ?>
                                        </div>
                                    <?php }?>                                
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php elseif ( $atts['style'] == 'style2' ): ?>
            <div class="container">
                <div class="box-video">
                    <div class="box-video-images">
                        <?php 
                            $img = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box'], 'thumb_size' => $image_size_large ) );     
                        ?>      
                        <?php if( $atts['image_box'] ) { ?>
                            <div class="box-image">
                                <?php echo $img['thumbnail']; ?>
                            </div>
                        <?php } ?>

                        <?php if ( trim( $video_url['url'] ) != '' ) { ?>
                            <button type="button" class="btn btn-video" data-toggle="modal" data-target="#modal-video">
                                <span class="sr-only sr-only-focusable"><?php echo esc_html( $video_url['title'] );?></span>
                                <span aria-hidden="true" class="icon-control-play"></span>                    
                            </button>             
                            <div class="modal fade trebol_popup_modal" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="Title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">                            
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span class="icon ion-android-close"></span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?php if($video_url['url'] ){?>
                                                <div class="embed-responsive-16by9  post-video single-video embed-responsive-16by9">
                                                    <?php $sp_video = $video_url['url'] ; ?>
                                                    <?php if(wp_oembed_get( $sp_video )) { ?>
                                                        <?php  echo nano_oembed_get($sp_video, 0,'nano-video'); ?>
                                                    <?php } ?>
                                                </div>
                                            <?php }?>                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="box-video-intro <?php echo esc_attr( $atts['align'] ); ?>">
                        <div class="box-video-intro-wrap">
                            <?php if ( trim( $atts['title'] ) != '' ) { ?>
                                <h2 class="widgettitle box-title">
                                    <span><?php echo trim( $atts['title'] ); ?></span>
                                </h2>
                            <?php } ?>

                            <?php if ( trim( $atts['description'] ) != '' ) { ?>
                                <div class="box-description">
                                    <?php echo trim( $atts['description'] ); ?>
                                </div>
                            <?php } ?>

                            <?php if ( trim( $url['url'] ) != '' ) { ?>
                                <div class="box-button-link">
                                    <a class="btn btn-link-primary" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>                
            </div>
        <?php else: ?>            
            <div class="box-video">
                <div class="box-video-images">
                    <?php 
                        $img = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box'], 'thumb_size' => $image_size_full ) );     
                    ?>      
                    <?php if( $atts['image_box'] ) { ?>
                        <div class="box-image">
                            <?php echo $img['thumbnail']; ?>
                        </div>
                    <?php } ?>

                    <?php if ( trim( $video_url['url'] ) != '' ) { ?>
                        <button type="button" class="btn btn-video" data-toggle="modal" data-target="#modal-video">
                            <span class="sr-only sr-only-focusable"><?php echo esc_html( $video_url['title'] );?></span>
                            <span aria-hidden="true" class="icon-control-play"></span>                    
                        </button>             
                        <div class="modal fade trebol_popup_modal" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="Title" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">                            
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span class="icon ion-android-close"></span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php if($video_url['url'] ){?>
                                            <div class="post-video single-video embed-responsive embed-responsive-16by9">
                                                <div class="embed-responsive-item">
                                                    <?php $sp_video = $video_url['url'] ; ?>
                                                    <?php if(wp_oembed_get( $sp_video )) { ?>
                                                        <?php  echo nano_oembed_get($sp_video, 0,'nano-video'); ?>
                                                    <?php } ?>                                                    
                                                </div>
                                            </div>
                                        <?php }?>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>               
        <?php endif ?>
    </div>   
</div>

