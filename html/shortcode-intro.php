<?php

/**
* The default template for displaying content
*
* @author      NanoAgency
* @link        http://nanoagency.co
* @copyright   Copyright (c) 2018 NanoAgency
* @license     GPL v2
*/

$url        = vc_build_link( $atts['url'] );
$image_size = array(470,520);
$css_class  = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$images     = explode(",",$atts['images']);

?>

<div class="block-intro na-block <?php echo esc_attr( $atts['el_class'] ).' '.esc_attr( $css_class ); ?>"> 
    <div class="na-block-content">
        <div class="na-block-intro">
            <?php if( $images) { ?>
                <div class="slider-nav">
                    <button type="button" class="prev-slide"><i class="icon ion-android-arrow-back"></i><span><?php echo esc_html__( 'Previous', 'trebol' ); ?></span></button><button type="button" class="next-slide"><span><?php echo esc_html__( 'Next', 'trebol' ); ?></span><i class="icon ion-android-arrow-forward"></i></button>                            
                </div>
                <ul class="block-intro-carousel list-unstyled">
                    <?php foreach( $images as $image ){
                        $img = wpb_getImageBySize( array( 'attach_id' => (int) $image, 'thumb_size' => $image_size ) );      
                     ?>
                        <li class="box-image">
                            <?php echo $img['thumbnail']; ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>                        

        <div class="na-block-intro-content <?php echo esc_attr( $atts['title_align'] ); ?>">
            <div class="box-content">
                <?php if ( isset( $atts['title'] ) ) { ?>
                    <h2 class="widgettitle box-title">                        
                        <?php echo trim( $atts['title'] ); ?>
                    </h2>
                <?php } ?>        
                <?php if ( isset( $atts['description'] ) ) { ?>
                    <div class="box-description">                                            
                        <?php echo trim( $atts['description'] ); ?>
                    </div>
                <?php } ?>        
                <?php if ( isset( $url['url'] ) ) { ?>
                    <div class="box-button-link">
                        <a class="btn btn-link-primary" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>   
</div>
