<?php

/**
* The default template for displaying content
*
* @author      NanoAgency
* @link        http://nanoagency.co
* @copyright   Copyright (c) 2018 NanoAgency
* @license     GPL v2
*/

$image_size = array(50,50);
$css_class  = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items      = (array) vc_param_group_parse_atts($atts['items']);

?>

<div class="block-feature-services na-block na-block-feature-services <?php echo esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] ).' '.esc_attr( $css_class ); ?>"> 
    <div class="na-block-content">
        <div class="row">
            <?php foreach( $items as $item ): ?>      
                <?php $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) ); ?>                
                <?php if( $atts['style']     =='style1') : ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">   
                        <div class="box-feature-services">
                            <?php if( $item['image_box'] ) { ?>
                                <div class="box-image">
                                    <?php echo $img['thumbnail']; ?>
                                </div>
                            <?php } ?>  
                                                      
                            <?php if ( trim( $item['title'] ) != '' ) { ?>
                                <h3 class="box-title">                                    
                                    <?php echo trim($item['title']); ?>
                                </h3>
                            <?php } ?>

                            <?php if ( trim( $item['description'] ) != '' ) { ?>
                                <div class="box-description">                                    
                                    <?php echo trim($item['description']); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>  
                <?php elseif( $atts['style'] =='style2' ): ?>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-6">   
                        <div class="box-feature-services">
                            <?php if( $item['image_box'] ) { ?>
                                <div class="box-image">
                                    <?php echo $img['thumbnail']; ?>
                                </div>
                            <?php } ?>

                            <?php if ( trim( $item['title'] ) != '' ) { ?>
                                <h3 class="box-title">                                    
                                    <?php echo trim($item['title']); ?>
                                </h3>
                            <?php } ?>

                            <?php if ( trim( $item['description'] ) != '' ) { ?>
                                <div class="box-description">                                    
                                    <?php echo trim($item['description']); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>  
                <?php else: ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">   
                        <div class="box-feature-services">
                            <?php if( $item['image_box'] ) { ?>
                                <div class="box-image">
                                    <?php echo $img['thumbnail']; ?>
                                </div>
                            <?php } ?>
                            
                            <?php if ( trim( $item['title'] ) != '' ) { ?>
                                <h3 class="box-title">                                    
                                    <?php echo trim($item['title']); ?>
                                </h3>
                            <?php } ?>

                            <?php if ( trim( $item['description'] ) != '' ) { ?>
                                <div class="box-description">                                    
                                    <?php echo trim($item['description']); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>  
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>   
</div>