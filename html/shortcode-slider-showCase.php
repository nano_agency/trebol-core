<?php

//small large

$idran        = random_int(0,99);
/*$image_size = array(985,720);*/
$image_size   = array(1045,836);
$items        = (array) vc_param_group_parse_atts($atts['items']);

?>

<div class="block-slider-showCase na-block">
    <?php if ( $atts['block_title'] ) {?>
        <h3 class="block-title">
            <?php echo esc_html( $atts['block_title'] ); ?>
        </h3>
    <?php }?>
    <div class="na-block-content row clearfix" data-number="1">
            <div class="ground-header-tabs">
                    <ul class="flex-column header-nav nav-pills nav nav-tabs" role="tablist" aria-orientation="vertical">
                        <?php $count = 0; ?>
                        <?php foreach( $items as $item ): ?>
                            <li class="ground-tabs nav-item <?php echo ($count == 0)?'active':''; ?>" id="tab-<?php echo esc_attr($count);?>" href="#section-<?php echo esc_attr($count);?>" data-toggle="pill" role="tab" aria-controls="section-<?php echo esc_attr($count);?>" aria-selected="<?php echo ($count==0)?'true':'false'; ?>">
                                <?php if($item['title_box']) { ?>
                                    <h3 class="box-title">
                                        <?php echo esc_html($item['title_box']);?>
                                    </h3>
                                <?php }?>
                                <?php if($item['sub_box'] && $item['link_box'] ) { ?>
                                    <div class="box-sub">
                                        <a href="<?php echo esc_html($item['link_box']);?>">
                                            <?php echo esc_html($item['sub_box']);?>
                                        </a>
                                    </div>
                                <?php }?>

                                <?php if(isset($item['bg_box'])) { ?>
                                    <span class="bgr-bg" style="background: <?php echo esc_attr($item['bg_box']);?>"></span>
                                <?php }?>

                            </li>
                            <?php $count++; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="tab-content ground-content-tabs" id="v-pills-tabContent">
                    <?php $count = 0; ?>
                    <?php foreach( $items as $item ): ?>
                        <div class="content-box tab-panes fade <?php echo ( $count == 0 )?' active in':''; ?> " id="section-<?php echo esc_attr($count);?>" role="tabpanel" aria-labelledby="tab-<?php echo esc_attr($count);?>">

                            <?php
                            $sliders = (array) vc_param_group_parse_atts($item['slider_box']);
                            ?>
                            <ul class="list-slider tab-carousel" data-number="1" data-auto="false" data-pagination="false">
                                <?php foreach( $sliders as $slider ): ?>
                                    <li class="slider-item">

                                        <?php if( isset($slider['image_slider'])) {
                                            $img = wpb_getImageBySize( array( 'attach_id' => (int) $slider['image_slider'], 'thumb_size' => $image_size ) );
                                            ?>
                                            <div class="box-image" style="background-image:url(<?php echo esc_url($img['p_img_large'][0]);?>)">
                                            </div>
                                        <?php } ?>
                                            <div class="ground-title">
                                                <div class="slide-count-wrap">
                                                    <span class="current"></span>
                                                    <span class="total"></span>
                                                </div>

                                                <?php if( isset( $slider['title_slider'] ) && !empty( $slider['title_slider'] ) ) { ?>
                                                    <h5 class="title-slider">                                                        
                                                        <?php echo trim( $slider['title_slider'] ); ?>
                                                    </h5>
                                                <?php }?>

                                                <?php if(isset($slider['link_slider'])) { ?>
                                                    <?php
                                                        $link = trim( $slider['link_slider'] );
                                                        $link = ( '||' === $link ) ? '' : $link;
                                                        $link = vc_build_link( $link );
                                                    ?>
                                                    <?php if ( strlen( $link['url'] ) > 0 ) { ?>
                                                    <div class="box-button-link">
                                                        <a class="btn btn-link" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                                                    </div>
                                                    <?php }?>
                                                </div>
                                       <?php }?>
                                    </li>
                                 <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php $count++; ?>
                    <?php endforeach; ?>
                </div>

    </div>
    <?php if ( $atts['link_facebook'] || $atts['link_twitter']  || $atts['link_instagram']){?>
        <ul class="block-share">
            <?php if ( $atts['link_facebook']){?>
                <li class="social-item">
                    <a href="<?php echo esc_html( $atts['link_facebook'] ); ?>" target="_blank">
                        <?php echo esc_html('Facebook','nano'); ?>
                    </a>
                </li>
            <?php }?>
            <?php if ( $atts['link_twitter']){?>
                <li class="social-item">
                    <a href="<?php echo esc_html( $atts['link_twitter'] ); ?>" target="_blank">
                        <?php echo esc_html('Twitter','nano'); ?>
                    </a>
                </li>
            <?php }?>
            <?php if ( $atts['link_google']){?>
                <li class="social-item">
                    <a href="<?php echo esc_html( $atts['link_google'] ); ?>" target="_blank">
                        <?php echo esc_html('Google Plus','nano'); ?>
                    </a>
                </li>
            <?php }?>
            <?php if ( $atts['link_linkedin']){?>
                <li class="social-item">
                    <a href="<?php echo esc_html( $atts['link_linkedin'] ); ?>" target="_blank">
                        <?php echo esc_html('LinkedIn','nano'); ?>
                    </a>
                </li>
            <?php }?>
            <?php if ( $atts['link_instagram']){?>
                <li class="social-item">
                    <a href="<?php echo esc_html( $atts['link_instagram'] ); ?>" target="_blank">
                        <?php echo esc_html('Instagram','nano'); ?>
                    </a>
                </li>
            <?php }?>
            <?php if ( $atts['link_dribbble']){?>
                <li class="social-item">
                    <a href="<?php echo esc_html( $atts['link_dribbble'] ); ?>" target="_blank">
                        <?php echo esc_html('Dribbble','nano'); ?>
                    </a>
                </li>
            <?php }?>
            <?php if ( $atts['link_pinterest']){?>
                <li class="social-item">
                    <a href="<?php echo esc_html( $atts['link_pinterest'] ); ?>" target="_blank">
                        <?php echo esc_html('pinterest','nano'); ?>
                    </a>
                </li>
            <?php }?>

        </ul>
    <?php }?>
</div>

