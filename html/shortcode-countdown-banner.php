<?php

/**
* The default template for displaying content
*
* @author      NanoAgency
* @link        http://nanoagency.co
* @copyright   Copyright (c) 2018 NanoAgency
* @license     GPL v2
*/

$url             = vc_build_link( $atts['url'] );
$image_size      = array(952,517);
wp_enqueue_script( 'countdown-timer-js' );
$css_class       = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$countdown_timer = str_replace( '/', '-', $atts['date'] );

?>

<div class="block-countdown-banner na-block na-block-countdown <?php echo esc_attr( $atts['el_class'] ).' '.esc_attr( $css_class ); ?>"> 
    <div class="na-block-content">
        <div class="box-countdown-banner">
            <div class="box-countdown-meta <?php echo esc_attr( $atts['align'] ); ?>">
                <div class="box-countdown-banner-wrap">
                    <?php if ( trim( $atts['title'] ) != '' ) { ?>
                        <h2 class="widgettitle box-title">
                            <span><?php echo trim( $atts['title'] ); ?></span>
                        </h2>
                    <?php } ?>

                    <?php if ( trim( $countdown_timer ) != '' ) { ?>
                        <div class="box-countdown-timer">
                            <div class="na-timer" data-end-date="<?php echo esc_attr( $countdown_timer ) ?>"></div>
                        </div>                        
                    <?php } ?>                   

                    <?php if ( trim( $atts['description'] ) != '' ) { ?>
                        <div class="box-description">
                            <?php echo trim( $atts['description'] ); ?>
                        </div>
                    <?php } ?>

                    <?php if ( trim( $url['url'] ) != '' ) { ?>
                        <div class="box-button-link">
                            <a class="btn btn-link-primary" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                        </div>
                    <?php } ?>
                </div>
            </div>            
            <div class="box-countdown-images">
                <?php 
                    $img = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box'], 'thumb_size' => $image_size ) );     
                ?>      
                <?php if( $atts['image_box'] ) { ?>
                    <div class="box-image">
                        <?php echo $img['thumbnail']; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>   
</div>