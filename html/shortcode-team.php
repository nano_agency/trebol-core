<?php

$image_size = array(720,800);
$css_class  = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items      = (array) vc_param_group_parse_atts($atts['items']);

?>

<div class="block-team na-block na-block-team <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] );?>">  
    <div class="na-block-content">         
        <div class="row">
            <?php foreach( $items as $item ): ?>                    
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <?php 
                        $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) );                          
                    ?>      
                    <div class="box-team">   
                        <?php if($item['image_box']) { ?>
                            <div class="box-image">
                                <?php echo $img['thumbnail']; ?>
                            </div>
                        <?php } ?>

                        <?php if ( isset($item['title']) && trim( $item['title'] ) != '' ) { ?>                            
                            <h2 class="box-title">
                                <span><?php echo trim( $item['title'] ); ?></span>
                            </h2>                 
                        <?php } ?>

                        <?php if ( isset($item['job_title']) && trim( $item['job_title'] ) != '' ) { ?>                            
                            <p class="box-subtitle">
                                <span><?php echo trim( $item['job_title'] ); ?></span>
                            </p>                 
                        <?php } ?>                        
                    </div> 
                </div>
            <?php endforeach; ?>   
        </div>        
    </div>
</div>