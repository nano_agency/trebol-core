<?php

$image_size_large = array(768,1013);
$image_size_small = array(740,640);
$css_class        = vc_shortcode_custom_css_class( $atts['css'], ' ' );

?>

<div class="block-banner-split na-banner-split na-block <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['position_layouts'] );?>">  
    <div class="na-block-content">         
        <div class="box-banner-split banner-split-large">
            <div class="box-banner-content">
                <?php 
                    $img  = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box_1'], 'thumb_size' => $image_size_large ) ); 
                    $link = trim( $atts['url_1'] );
                    $link = ( '||' === $link ) ? '' : $link;
                    $link = vc_build_link( $link );    
                ?>
                <?php if($atts['image_box_1']) { ?>
                    <div class="box-image">
                        <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                            <?php echo $img['thumbnail']; ?>
                        </a>
                    </div>
                <?php } ?>
                <?php if ( trim( $atts['title_1'] ) != '' ) { ?>
                    <h4 class="box-title">
                        <?php echo trim( $atts['title_1'] ); ?>
                    </h4>
                <?php } ?>
                <?php if ( trim( $atts['subtitle_1'] ) != '' ) { ?>
                    <h5 class="box-subtitle">
                        <?php echo trim( $atts['subtitle_1'] ); ?>
                    </h5>
                <?php } ?>
            </div>
        </div>                                                        
        <div class="box-banner-split banner-split-small">
            <div class="box-banner-content">
                <?php 
                    $img  = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box_2'], 'thumb_size' => $image_size_small ) ); 
                    $link = trim( $atts['url_2'] );
                    $link = ( '||' === $link ) ? '' : $link;
                    $link = vc_build_link( $link );    
                ?>
                <?php if($atts['image_box_2']) { ?>
                    <div class="box-image">
                        <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                            <?php echo $img['thumbnail']; ?>
                        </a>
                    </div>
                <?php } ?>
                <?php if ( trim( $atts['title_2'] ) != '' ) { ?>
                    <h4 class="box-title">
                        <?php echo trim( $atts['title_2'] ); ?>
                    </h4>
                <?php } ?>
                <?php if ( strlen( $link['url'] ) > 0 ) { ?>
                    <div class="box-button-link">
                        <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                            <span><?php echo esc_html($link['title']);?></span>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>                                             
    </div>
</div>