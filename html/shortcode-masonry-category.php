<?php

$image_size = array(434,336);
$css_class  = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items      = (array) vc_param_group_parse_atts($atts['items']);

?>

<?php if( isset($atts['style']) && $atts['style']=='style1' ): ?>
<div class="block-masonry-category na-block na-masonry-category <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] ) ;?>">
    <div class="na-block-content affect-isotope items-masonry clearfix">
            <div class="grid-sizer"></div>
            <?php 
            $na         = 1; 
            $add_class  = 'small';         
            foreach ( $items as $item ) {?>
                <?php
                    if ( $na %2 ==0){
                        $image_size=array(580,320);
                        $add_class='small';
                    }                    
                    else{
                        $image_size=array(580,580);
                        $add_class='large';
                    }
                    $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) );

                    $link = trim( $item['link_box'] );
                    $link = ( '||' === $link ) ? '' : $link;
                    $link = vc_build_link( $link );    
                ?>

                <div class="<?php echo esc_attr( $add_class );?> box-masonry col-item clearfix">
                    <div class="box-content">       
                        <?php if($item['image_box']) { ?>
                            <div class="box-image">
                                <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                    <?php echo $img['thumbnail']; ?>
                                </a>
                            </div>
                        <?php } ?>
                        <div class="box-content-inner">
                            <?php
                                $html = '';
                                if($item['title_box']){
                                    $html .= '<h3 class="box-title"> <a href="'.$link['url'].'">'. esc_html($item['title_box']) .'</a></h3>';
                                }
                                if(isset($item['content_box'])){
                                    $html .= '<div class="box-description">'. esc_html($item['content_box']) .'</div>';
                                }                            

                                echo $html;

                                if ( strlen( $link['url'] ) > 0 ) { ?>
                                    <div class="box-button-link">
                                        <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                                    </div>
                                <?php }                                
                            ?>
                        </div>
                    </div>
                </div>
            <?php $na++; }
            ?>
    </div>
</div>
<?php elseif( isset($atts['style']) && $atts['style']=='style2' ): ?>
<div class="block-masonry-category na-block na-masonry-category <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] ) ;?>">
    <div class="na-block-content affect-isotopess items-masonry clearfix">
        <div class="grid-sizer"></div>            
        <?php     
        $nao         = 1;   
        $add_class  = 'small';    

        foreach ( $items as $item ) {?>
            <?php
                if ( $nao ==1 || $nao %4 == 0 || ($nao-1) %4 == 0){
                    $image_size=array(770,380);
                    $add_class='large';
                } 
                else{
                    $image_size=array(370,380);
                    $add_class='small';
                }

                $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) );

                $link = trim( $item['link_box'] );
                $link = ( '||' === $link ) ? '' : $link;
                $link = vc_build_link( $link );    
            ?>

            <div class="<?php echo esc_attr( $add_class );?> box-masonry col-item clearfix">
                <div class="box-content">       
                    <?php if($item['image_box']) { ?>
                        <div class="box-image">
                            <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                <?php echo $img['thumbnail']; ?>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="box-content-inner">
                        <?php
                            $html = '';
                            if($item['title_box']){
                                $html .= '<h3 class="box-title"> <a href="'.$link['url'].'">'. esc_html($item['title_box']) .'</a></h3>';
                            }
                            if(isset($item['content_box'])){
                                $html .= '<div class="box-description">'. esc_html($item['content_box']) .'</div>';
                            }                            

                            echo $html;

                            if ( strlen( $link['url'] ) > 0 ) { ?>
                                <div class="box-button-link">
                                    <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                                </div>
                            <?php }                                
                        ?>
                    </div>
                </div>
            </div>
        <?php $nao++; }
        ?>
    </div>
</div>
<?php elseif( isset($atts['style']) && $atts['style']=='style3' ): ?>
<div class="block-masonry-category na-block na-masonry-category <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] ) ;?>">
    <div class="na-block-content affect-isotope-cats items-masonry">
        <div class="grid-sizer"></div>
        <?php     
        $nao         = 1;
        $image_size = array(770,670);
        $add_class  = 'small';
        foreach ( $items as $item ) { ?>
            <?php
                if ( $nao ==1 || ($nao+1) %6 == 0 || ($nao-1) %6 == 0){                    
                    $image_size=array(840,687);
                    $add_class='large';
                } 
                else{
                    $image_size=array(370,300);
                    $add_class='small';
                }
                $img  = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) );
                $link = trim( $item['link_box'] );
                $link = ( '||' === $link ) ? '' : $link;
                $link = vc_build_link( $link );    
            ?>
            <div class="<?php echo esc_attr( $add_class );?> box-masonry col-item">
                <div class="box-content">       
                    <?php if($item['image_box']) { ?>
                        <div class="box-image">
                            <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                <?php echo $img['thumbnail']; ?>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="box-content-inner">
                        <?php if( $item['title_box'] ) : ?>
                            <h3 class="box-title"> 
                                <a href="<?php echo esc_url( $link['url'] ); ?>">                                    
                                    <?php echo trim( $item['title_box'] ); ?>
                                </a>
                            </h3>
                        <?php endif; ?>
                        <?php if( isset($item['content_box']) ) : ?>
                            <div class="box-description">                                
                                <?php echo trim( $item['content_box'] ); ?>
                            </div>
                        <?php endif; ?>
                        <?php if( strlen( $link['url'] ) > 0 ) : ?>
                            <div class="box-button-link">
                                <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php $nao++; 
        } ?>        
    </div>
</div>
<?php elseif( isset($atts['style']) && $atts['style']=='style4' ): ?>
<div class="block-masonry-category na-block na-masonry-category <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] ) ;?>">
    <div class="na-block-content affect-isotope-cats items-masonry">
        <div class="grid-sizer"></div>
        <?php     
        $nao        = 1;
        $image_size = array(770,670);
        $add_class  = 'small';
        foreach ( $items as $item ) { ?>
            <?php
                if ( $nao == 1 ){                    
                    $image_size=array(1000,847);
                    $add_class='large';
                } 
                elseif ($nao == 4 || $nao== 6){
                    $image_size=array(1000,411);
                    $add_class='large';                    
                }
                else{
                    $image_size=array(1000,847);
                    $add_class='small';
                }
                $img  = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) );
                $link = trim( $item['link_box'] );
                $link = ( '||' === $link ) ? '' : $link;
                $link = vc_build_link( $link );    
            ?>
            <div class="<?php echo esc_attr( $add_class );?> box-masonry col-item">
                <div class="box-content">       
                    <?php if($item['image_box']) { ?>
                        <div class="box-image">
                            <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                <?php echo $img['thumbnail']; ?>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="box-content-inner">
                        <?php if( $item['title_box'] ) : ?>
                            <h3 class="box-title"> 
                                <a href="<?php echo esc_url( $link['url'] ); ?>">                                    
                                    <?php echo trim( $item['title_box'] ); ?>
                                </a>
                            </h3>
                        <?php endif; ?>
                        <?php if( isset($item['content_box']) ) : ?>
                            <div class="box-description">                                
                                <?php echo trim( $item['content_box'] ); ?>
                            </div>
                        <?php endif; ?>
                        <?php if( strlen( $link['url'] ) > 0 ) : ?>
                            <div class="box-button-link">
                                <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php $nao++; 
        } ?>        
    </div>
</div>

<?php else:?>
<div class="block-masonry-category na-block na-masonry-category <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] ) ;?>">
    <div class="na-block-content affect-isotope items-masonry clearfix">
            <div class="grid-sizer"></div>
            <?php 
            $na         = 1; 
            $add_class  = 'small';         
            foreach ( $items as $item ) {?>
                <?php
                    if ( $na %2 ==0){
                        $image_size=array(580,320);
                        $add_class='small';
                    }                    
                    else{
                        $image_size=array(580,580);
                        $add_class='large';
                    }
                    $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) );

                    $link = trim( $item['link_box'] );
                    $link = ( '||' === $link ) ? '' : $link;
                    $link = vc_build_link( $link );    
                ?>

                <div class="<?php echo esc_attr( $add_class );?> box-masonry col-item clearfix">
                    <div class="box-content">       
                        <?php if($item['image_box']) { ?>
                            <div class="box-image">
                                <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                    <?php echo $img['thumbnail']; ?>
                                </a>
                            </div>
                        <?php } ?>
                        <div class="box-content-inner">
                            <?php
                                $html = '';
                                if($item['title_box']){
                                    $html .= '<h3 class="box-title"> <a href="'.$link['url'].'">'. esc_html($item['title_box']) .'</a></h3>';
                                }
                                if(isset($item['content_box'])){
                                    $html .= '<div class="box-description">'. esc_html($item['content_box']) .'</div>';
                                }                            

                                echo $html;

                                if ( strlen( $link['url'] ) > 0 ) { ?>
                                    <div class="box-button-link">
                                        <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                                    </div>
                                <?php }                                
                            ?>
                        </div>
                    </div>
                </div>
            <?php $na++; }
            ?>
    </div>
</div>
<?php endif;?>