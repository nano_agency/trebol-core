<?php

/**
* The default template for displaying content
*
* @author      NanoAgency
* @link        http://nanoagency.co
* @copyright   Copyright (c) 2018 NanoAgency
* @license     GPL v2
*/

$url       = vc_build_link( $atts['url'] );
$css_class = vc_shortcode_custom_css_class( $atts['css'], ' ' );

?>

<div class="na-block block-title-heading <?php echo esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] ).' '.esc_attr( $atts['align'] ).' '.esc_attr( $css_class ); ?>"> 
    <div class="na-block-content">
        <?php if( $atts['style']     =='style1') : ?>
            <?php if ( trim( $atts['title'] ) != '' ) { ?>
                <h2 class="widgettitle box-title">
                    <span><?php echo trim( $atts['title'] ); ?></span>
                </h2>
            <?php } ?>

            <?php if ( trim( $atts['subtitle'] ) != '' ) { ?>
                <div class="box-subtitle">
                    <?php echo trim( $atts['subtitle'] ); ?>
                </div>
            <?php } ?>

            <?php if ( trim( $atts['description'] ) != '' ) { ?>
                <div class="box-content <?php echo esc_attr( $atts['align'] ); ?>">
                    <div class="box-description">
                        <?php echo trim( $atts['description'] ); ?>
                    </div>
                </div>
            <?php } ?>

            <?php if ( trim( $url['url'] ) != '' ) { ?>
                <div class="box-button-link">
                    <a class="btn btn-link-primary" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                </div>
            <?php } ?>
        <?php elseif( $atts['style'] =='style2' ): ?>
            <?php if ( trim( $atts['subtitle'] ) != '' ) { ?>
                <h3 class="box-subtitle">
                    <?php echo trim( $atts['subtitle'] ); ?>
                </h3>
            <?php } ?>
            <?php if ( trim( $atts['title'] ) != '' ) { ?>
                <h2 class="widgettitle box-title">
                    <span><?php echo trim( $atts['title'] ); ?></span>
                </h2>
            <?php } ?>
            <?php if ( trim( $atts['description'] ) != '' ) { ?>                
                <div class="box-description">
                    <?php echo trim( $atts['description'] ); ?>
                </div>                
            <?php } ?>
            <?php if ( trim( $url['url'] ) != '' ) { ?>
                <div class="box-button-link">
                    <a class="btn btn-link-outline" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                </div>
            <?php } ?>
        <?php elseif( $atts['style'] =='style3' ): ?>
            <?php if ( trim( $atts['subtitle'] ) != '' ) { ?>
                <h3 class="box-subtitle">
                    <?php echo trim( $atts['subtitle'] ); ?>
                </h3>
            <?php } ?>
            <?php if ( trim( $atts['title'] ) != '' ) { ?>
                <h2 class="widgettitle box-title">
                    <span><?php echo trim( $atts['title'] ); ?></span>
                </h2>
            <?php } ?>
            <?php if ( trim( $atts['description'] ) != '' ) { ?>                
                <div class="box-description">
                    <?php echo trim( $atts['description'] ); ?>
                </div>                
            <?php } ?>
            <?php if ( trim( $url['url'] ) != '' ) { ?>
                <div class="box-button-link">
                    <a class="btn btn-link-outline" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                </div>
            <?php } ?>  
        <?php else: ?>
            <?php if ( trim( $atts['title'] ) != '' ) { ?>
                <h2 class="widgettitle box-title">
                    <span><?php echo trim( $atts['title'] ); ?></span>
                </h2>
            <?php } ?>

            <?php if ( trim( $atts['subtitle'] ) != '' ) { ?>
                <div class="box-subtitle">
                    <?php echo trim( $atts['subtitle'] ); ?>
                </div>
            <?php } ?>

            <?php if ( trim( $atts['description'] ) != '' ) { ?>
                <div class="box-content <?php echo esc_attr( $atts['align'] ); ?>">
                    <div class="box-description">
                        <?php echo trim( $atts['description'] ); ?>
                    </div>
                </div>
            <?php } ?>

            <?php if ( trim( $url['url'] ) != '' ) { ?>
                <div class="box-button-link">
                    <a class="btn btn-link" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> <?php echo esc_html( $url['title'] );?> </a>
                </div>
            <?php } ?>            
        <?php endif; ?>
    </div>   
</div>