<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$IDcats = explode(',',$atts['category']);
?>
<div class="block-category-tabs na-block">
    <div class="na-block-content">
        <?php if ( $atts['box_title'] ) {?>
            <h3 class="widgettitle box-title <?php echo  $atts['title_align'];?>">
                <span><?php echo htmlspecialchars_decode( $atts['box_title'] ); ?></span>
            </h3>
        <?php }?>

        <div data-example-id="togglable-tabs" role="tabpanel" class="cat-tabs box-content">
            <div class="cat-title box-tab-title <?php echo  $atts['title_align'];?>">
                <ul role="tablist" class="nav nav-tabs box-tabs <?php echo $atts['style']; ?>">
                    <li class="active" role="presentation">
                        <a class="tabs-title-cat" aria-expanded="true" data-toggle="tab" role="tab" href="#cat-all">
                            <?php echo esc_html__('All','nano'); ?>
                        </a>
                    </li>
                    <?php
                        $count=0;
                        foreach ($IDcats as $IDcat) {
                            $name_cat = get_term_by( 'slug', $IDcat, 'product_cat' );
                        ?>
                        <li class="box-tab-items" role="presentation">
                            <a class="box-tab-title tabs-title-cat" aria-expanded="true" data-toggle="tab" role="tab" href="#cat-<?php echo  $IDcat; ?>">
                                <span><?php echo $name_cat->name; ?></span>
                            </a>
                        </li>
                        <?php
                        $count++;
                        }
                    ?>
                </ul>
            </div>
            <div class="box-tab-content tab-content clearfix" id="tabs_product">
                <?php $count=0;
                $args['tax_query'] = array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'slug',
                    'order_by'  =>'id',
                    'terms'     => $IDcats
                );
                $atts['tax_query']  = $args;
                ?>
                <div  id="cat-all" class="tab-pane active" role="tabpanel">
                    <?php $output = nano_template_part('box-layouts/shortcode', 'product-'.$atts['box_layouts'], array('atts' => $atts)); ?>
                </div>

                <?php foreach ($IDcats as $IDcat) {
                        $args['tax_query'] = array(
                            'taxonomy'  => 'product_cat',
                            'field'     => 'slug',
                            'order_by'  =>'id',
                            'terms'     => $IDcat
                        );
                        $atts['tax_query']  = $args;
                    ?>
                    <div id="cat-<?php echo $IDcat;?>" class="tab-pane" role="tabpanel">
                        <?php $output = nano_template_part('box-layouts/shortcode', 'product-'.$atts['box_layouts'], array('atts' => $atts)); ?>
                    </div>
                    <?php $count++;
                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

