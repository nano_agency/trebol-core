<?php

//small large
$image_size        = array(740,760);
$image_size_medium = array(370,420);
$css_class         = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items             = (array) vc_param_group_parse_atts($atts['items']);

?>

<div class="block-category-banners na-block <?php echo esc_attr( $css_class ).' '.esc_attr( $atts['el_class'] ).' '.esc_attr( $atts['style'] );?>">  
    <div class="na-block-content">         
        <div class="row">
            <?php if ( $atts['style'] =='style1' ): ?>
                <?php foreach( $items as $item ): ?>                    
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <?php 
                            $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) ); 
                            $link = trim( $item['url'] );
                            $link = ( '||' === $link ) ? '' : $link;
                            $link = vc_build_link( $link );    
                        ?>      
                        <div class="box-category-banners">   
                            <?php if($item['image_box']) { ?>
                                <div class="box-image">
                                    <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                        <?php echo $img['thumbnail']; ?>
                                    </a>
                                </div>
                            <?php } ?>

                            <?php if ( strlen( $link['url'] ) > 0 ) { ?>
                                <div class="box-button-link">
                                    <a class="btn btn-light" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                                        <span><?php echo esc_html($link['title']);?></span>
                                        <?php if ( isset($item['number']) ) { ?>
                                            <sup class="box-lable"><?php echo '(' .' '. esc_html($item['number']) .' '. ')'; ?></sup>                            
                                        <?php } ?>
                                    </a>
                                </div>
                            <?php } ?>
                        </div> 
                    </div>
                <?php endforeach; ?>            
            <?php else: ?>
                <?php foreach( $items as $item ): ?>                    
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <?php 
                            $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size_medium ) ); 
                            $link = trim( $item['url'] );
                            $link = ( '||' === $link ) ? '' : $link;
                            $link = vc_build_link( $link );    
                        ?>      
                        <div class="box-category-banners">   
                            <?php if($item['image_box']) { ?>
                                <div class="box-image">
                                    <a href="<?php echo esc_url($link['url']); ?>" class="box-image-link">
                                        <?php echo $img['thumbnail']; ?>
                                    </a>
                                </div>
                            <?php } ?>
                            <div class="box-category-banner-wrap">                                
                                <?php if ( isset($item['title']) ) { ?>
                                    <h4 class="box-title">
                                        <a class="box-title-link" href="<?php echo esc_url($link['url']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                                            <?php echo trim( $item['title'] ); ?>
                                        </a>                                        
                                    </h4>
                                <?php } ?>
                                <?php if ( strlen( $link['url'] ) > 0 ) { ?>
                                    <div class="box-button-link">
                                        <a class="btn btn-link-primary" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                                            <span><?php echo esc_html($link['title']);?></span>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>                        
                        </div> 
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>                           
        </div>        
    </div>
</div>
