<?php

global $query_shop;
$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );

?>

<div class="products_shortcode_wrap product-carousel">
    <div class="row">               
        <ul class="products-blocks na-carousel clearfix" data-number="<?php echo esc_attr( $atts['column'] );?>" data-pagination="<?php echo esc_attr( $atts['slider_dots'] ); ?>">
            <?php while ( $query_shop->have_posts() ){
                $query_shop->the_post();
                    global $product; ?>
                    <li <?php post_class('col-item'); ?>>
                        <?php wc_get_template_part( 'layouts/content-product-'.$atts['products_types']); ?>
                    </li>
                <?php
            }
            wp_reset_postdata();?>
        </ul>
    </div>
</div>

<?php

