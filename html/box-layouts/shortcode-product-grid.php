<?php
global $query_shop;

$typePagination         = get_theme_mod('trebol_woo_pagination',false);
$atts['posts_per_page'] = ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page');
$query_shop             = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );
$num_pages              = $query_shop->max_num_pages;

foreach($atts['tax_query'] as $key => $vals) {
    $cats=$vals['terms'];
    if(is_array($cats)){
        $cats=implode(",",$cats);;
    }
}

?>

<div class="products_shortcode_wrap items-grid type-tabShop main-content">
    <ul class="products-block tab-isotope row clearfix"
        data-col="<?php echo esc_attr($atts['column']);?>"
        data-pages="<?php echo esc_attr($num_pages);?>"
        data-number="<?php echo esc_attr($atts['number']);?>"
        data-cat="<?php echo esc_attr($cats);?>"
        data-layout="<?php echo esc_attr($atts['products_types'])?>">
        <?php while ( $query_shop->have_posts() ){
            $query_shop->the_post();?>
            <li <?php post_class('col-item'); ?>>
                <?php wc_get_template_part( 'layouts/content-product-'.$atts['products_types']); ?>
            </li>
        <?php }
            wp_reset_postdata();
        ?>
    </ul>
    <?php if($typePagination):?>
        <div class="infload-controls category-loadmore">
            <a href="#" class="loadShop infload-btn"><?php esc_html_e( 'load more', 'trebol' ); ?></a>
            <a href="#" class="infload-to-top hidden"><?php esc_html_e( 'All products loaded.', 'trebol' ); ?></a>
        </div>
    <?php endif;?>

    <?php if ( isset( $atts['url'])  && !empty( $atts['url'] )){ 
        $url = vc_build_link( $atts['url'] );
        ?>
        <div class="infload-controls">            
            <a class="btn btn-theme" href="<?php echo esc_url($url['url']);?>" target="<?php echo esc_attr( $url['target'] );?>"> 
                <?php echo esc_html( $url['title'] );?>                 
            </a>
        </div>
    <?php } ?>
</div>
