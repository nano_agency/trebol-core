<?php

if (!class_exists('NA_Custom_Post_Type_Member')) {
    class NA_Custom_Post_Type_Member
    {
        public static function &getInstance()
        {
            static $instance;
            if (!isset($instance)) {
                $instance = new NA_Custom_Post_Type_Member();
            }
            return $instance;
        }

        public function init() {
            add_action('init', array($this, 'register_member'));
            add_action('init', array($this, 'register_member_category'));
        }

        public function register_member()
        {
            $labels = array(
                'name' => __('NA Member', 'nano'),
                'singular_name' => __('member', 'nano'),
                'add_new' => __('Add New', 'nano'),
                'add_new_item' => __('Add New Member', 'nano'),
                'edit_item' => __('Edit Member', 'nano'),
                'new_item' => __('New Member', 'nano'),
                'view_item' => __('View Member', 'nano'),
                'search_items' => __('Search Member', 'nano'),
                'not_found' =>  __('No members have been added yet', 'nano'),
                'not_found_in_trash' => __('Nothing found in Trash', 'nano'),
                'parent_item_colon' => ''
            );

            $args = array(
                'labels' => $labels,
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'show_in_nav_menus' => false,
                'menu_icon'=> 'dashicons-welcome-view-site',
                'rewrite' => false,
                'supports' => array('title', 'editor'),
                'has_archive' => true,
            );

            register_post_type( 'member' , $args );
        }

        public function register_member_category()
        {
            $args = array(
                "label" 						=> __('Member Categories', 'nano'),
                "singular_label" 				=> __('Member Category', 'nano'),
                'public'                        => true,
                'hierarchical'                  => true,
                'show_ui'                       => true,
                'show_in_nav_menus'             => false,
                'args'                          => array( 'orderby' => 'term_order' ),
                'rewrite'                       => false,
                'query_var'                     => true
            );

            register_taxonomy( 'member_category', 'parallax', $args );
        }
    }

    NA_Custom_Post_Type_Member::getInstance()->init();
}