<?php
/**
 * Framework  Nano
 * @package     Nano
 * @version     1.0
 * @author      Nanotrebol
 * @link        http://www.nanotrebol.co
 * @copyright   Copyright (c) 2016 Nanotrebol
 * @license     GPL v2
 */


if (!function_exists('nano_get_part_wc')) {
    function nano_get_part_wc($slug = null, $name = null, array $params = array())
    {
        global $wp_query;
        $template_slug = 'woocommerce' . '/' . $slug;
        do_action("get_template_part_{$template_slug}", $template_slug, $name);

        $templates = array();
        $pluginTemplates = array();
        if (isset($name)){
            $templates[] = "{$template_slug}-{$name}.php";
            $pluginTemplates[] = "{$slug}-{$name}.php";
        }

        $templates[] = "{$template_slug}.php";
        $pluginTemplates[] = "{$slug}.php";

        $_template_file = locate_template($templates, false, false);
        if (is_array($wp_query->query_vars)) {
            extract($wp_query->query_vars, EXTR_SKIP);
        }
        extract($params, EXTR_SKIP);
        if (file_exists($_template_file)) {
            include($_template_file);
        }elseif((file_exists(NANO_PLUGIN_PATH . '/inc/' . $pluginTemplates[0]))){
            include(NANO_PLUGIN_PATH . '/inc/' . $pluginTemplates[0]);
        }
    }
}
if (!function_exists('na_part_templates')) {
    function na_part_templates($slug = null, $name = null, array $params = array())
    {
        global $wp_query;
        $slug = 'templates/'.$slug;
        do_action("get_template_part_{$slug}", $slug, $name);

        $templates = array();
        if (isset($name))
            $templates[] = "{$slug}-{$name}.php";
        $templates[] = "{$slug}.php";

        $_template_file = locate_template($templates, false, false);


        if (is_array($wp_query->query_vars)) {
            extract($wp_query->query_vars, EXTR_SKIP);
        }
        extract($params, EXTR_SKIP);


        if (file_exists($_template_file)) {
            require($_template_file);
        }
    }
}
if (!function_exists('nano_template_part')) {
    function nano_template_part($slug = null, $name = null, array $params = array())
    {
        global $wp_query;
        $template_slug = 'vc_templates' . '/' . $slug;
        do_action("get_template_part_{$template_slug}", $template_slug, $name);

        $templates = array();
        $pluginTemplates = array();
        if (isset($name)){
            $templates[] = "{$template_slug}-{$name}.php";
            $pluginTemplates[] = "{$slug}-{$name}.php";
        }

        $templates[] = "{$template_slug}.php";
        $pluginTemplates[] = "{$slug}.php";

        $_template_file = locate_template($templates, false, false);
        if (is_array($wp_query->query_vars)) {
            extract($wp_query->query_vars, EXTR_SKIP);
        }
        extract($params, EXTR_SKIP);

        if (file_exists($_template_file)) {
            include($_template_file);
        } elseif((file_exists(NANO_PLUGIN_PATH . '/html/' . $pluginTemplates[0]))){
            include(NANO_PLUGIN_PATH . '/html/' . $pluginTemplates[0]);
        }
    }
}

function nano_multi_select_categories($settings, $value, $taxonomies = 'category'){
    $param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
    $type = isset($settings['type']) ? $settings['type'] : '';
    $class = isset($settings['class']) ? $settings['class'] : '';
    $categories = get_terms( $taxonomies );

    $output = $selected = $ids = '';
    if ( $value !== '' ) {
        $ids = explode( ',', $value );
        $ids = array_map( 'trim', $ids );
    } else {
        $ids = array();
    }
    $output .= '<select class="nano-select-multi-category" multiple="multiple" style="min-width:200px;">';
    foreach($categories as $cat){
        if(in_array($cat->slug, $ids)){
            $selected = 'selected="selected"';
        } else {
            $selected = '';
        }
        $output .= '<option '.esc_attr($selected).' value="'. esc_attr($cat->slug) .'">'. esc_html__($cat->name,'na-nano') .'</option>';
    }
    $output .= '</select>';

    $output .= "<input type='hidden' name='". esc_attr($param_name) ."' value='".esc_attr( $value) ."' class='wpb_vc_param_value ". esc_attr($param_name) ." ".esc_attr($type) ." ". esc_attr($class) ."'>";
    $output .= '<script type="text/javascript">
							jQuery(".nano-select-multi-category").select({
								placeholder: "Select Categories",
								allowClear: true
							});
							jQuery(".nano-select-multi-category").on("change",function(){
								jQuery(this).next().val(jQuery(this).val());
							});
						</script>';
    return $output;

}

function vc_field_nano_multi_select($settings, $value){
    $param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
    $type = isset($settings['type']) ? $settings['type'] : '';
    $class = isset($settings['class']) ? $settings['class'] : '';
    $options = isset($settings['value']) ? $settings['value'] : array();

    $output = $selected = $ids = '';

    if ( $value !== '' ) {
        $ids = explode( ',', $value );
        $ids = array_map( 'trim', $ids );
    } else {
        $ids = array();
    }

    $output .= '<select class="nano-select-multi" multiple="multiple" style="min-width:200px;">';
    foreach($options as $name => $val ){

        if(in_array($val, $ids)){
            $selected = 'selected="selected"';
        } else {
            $selected = '';
        }
        $output .= '<option '. esc_attr($selected) .' value="'.esc_attr($val).'">'. esc_html__($name, 'na-nano') .'</option>';
    }
    $output .= '</select>';

    $output .= "<input type='hidden' name='". esc_attr($param_name) ."' value='". esc_attr($value) ."' class='wpb_vc_param_value ". esc_attr($param_name)." ".esc_attr($type)." ".esc_attr($class)."'>";
    $output .= '<script type="text/javascript">
							jQuery(".nano-select-multi").select({
								placeholder: "Select Categories",
								allowClear: true
							});
							jQuery(".nano-select-multi").on("change",function(){
								jQuery(this).next().val(jQuery(this).val());
							});
						</script>';
    return $output;
}

function vc_field_post_categories($settings, $value) {
    return nano_multi_select_categories($settings, $value, 'category');
}

function vc_field_portfolio_categories($settings, $value) {
    return nano_multi_select_categories($settings, $value, 'portfolio_category');
}

function vc_field_testimonial_categories($settings, $value) {
    return nano_multi_select_categories($settings, $value, 'testimonial_category');
}

function vc_field_product_categories($settings, $value) {
    return nano_multi_select_categories($settings, $value, 'product_cat');
}

function vc_field_image_radio($settings, $value) {
    $type = isset($settings['type']) ? $settings['type'] : '';
    $class = isset($settings['class']) ? $settings['class'] : '';
    $output = '<input class="wpb_vc_param_value '. esc_attr($settings['param_name']).' '.esc_attr($type).' '.esc_attr($class).'"  type="hidden" name="'.esc_attr($settings['param_name']).'" value="'.esc_attr($value).'">';
    $width = isset($settings['width']) ? $settings['width'] : '120px';
    $height = isset($settings['height']) ? $settings['height'] : '80px';
    if(count($settings['value']) > 0 ){
        foreach($settings['value'] as $param => $param_val) {
            $border_color = 'white';
            if($param_val == $value){
                $border_color = 'green';
            }
            $output .= '<img class="nano-image-radio-'.esc_attr($settings['param_name']).'" src="'.esc_url($param).'" data-value="'.esc_attr($param_val).'" style="width:'.esc_attr($width).';height:'.esc_attr($height).';border-style: solid;border-width: 5px;border-color: '.esc_attr($border_color).';margin-left:0px;">';
        }
        $output .= '<script type="text/javascript">
							jQuery(".nano-image-radio-'.esc_js($settings['param_name']).'").click(function() {
							    jQuery("input[name=\''.esc_js($settings['param_name']).'\']").val(jQuery(this).data("value"));
							    jQuery(".nano-image-radio-'.esc_js($settings['param_name']).'").css("border-color", "white");
							    jQuery(this).css("border-color", "green");
							});
						</script>';
    }
    return $output;
}


if (function_exists('vc_add_shortcode_param')){
    vc_add_shortcode_param('nano_post_categories', 'vc_field_post_categories');
    vc_add_shortcode_param('nano_portfolio_categories', 'vc_field_portfolio_categories');
    vc_add_shortcode_param('nano_testimonial_categories', 'vc_field_testimonial_categories');
    vc_add_shortcode_param('nano_product_categories', 'vc_field_product_categories');
    vc_add_shortcode_param('nano_image_radio', 'vc_field_image_radio');
    vc_add_shortcode_param('nano_multi_select', 'vc_field_nano_multi_select');
}

// Author Link Social
function na_social_author( $contactmethods ) {
    $contactmethods['twitter']   = 'Twitter Username';
    $contactmethods['facebook']  = 'Facebook Username';
    $contactmethods['google']    = 'Google Plus Username';
    $contactmethods['instagram'] = 'Instagram Username';
    $contactmethods['pinterest'] = 'Pinterest Username';
    return $contactmethods;
}
add_filter('user_contactmethods','na_social_author',10,1);

/* Count share =======================================================================================================*/
if(!function_exists('share_count')){
    function share_count( $url ) {
        $count_face=facebook_like_share_count($url);
        $count_twitter=twitter_tweet_count($url);
        $count_linkedin=linkedin_count($url);
        $count_pinterest=pinterest_count($url);
        $count_google=google_count($url);
        $count=$count_face + $count_twitter + $count_linkedin + $count_pinterest + $count_google;
        return $count;
    };
}
function facebook_like_share_count( $url ) {
    global $wp_filesystem;
    $api ='http://graph.facebook.com/?id=' . $url ;

    if( empty( $wp_filesystem ) ) {
        require_once( ABSPATH .'/wp-admin/includes/file.php' );
        WP_Filesystem();
    }
    if( $wp_filesystem ) {
        $count_face=$wp_filesystem->get_contents($api);
    }
    if($count_face){
        $count = json_decode( $count_face );
        return $count->share->share_count;
    }
    return false;
};

function twitter_tweet_count( $url ) {
    global $wp_filesystem;
    $api ='http://public.newsharecounts.com/count.json?url=' . $url;
    if( empty( $wp_filesystem ) ) {
        require_once( ABSPATH .'/wp-admin/includes/file.php' );
        WP_Filesystem();
    }
    if( $wp_filesystem ) {
        $count_tweet=$wp_filesystem->get_contents($api);
    }
    if($count_tweet){
        $count = json_decode( $count_tweet );
        return $count->count;
    }
    return false;
};

function linkedin_count( $url ) {
    global $wp_filesystem;
    $api ='https://www.linkedin.com/countserv/count/share?url=' . urlencode( $url ).'&format=json';
    if( empty( $wp_filesystem ) ) {
        require_once( ABSPATH .'/wp-admin/includes/file.php' );
        WP_Filesystem();
    }
    if( $wp_filesystem ) {
        $count_linkedin=$wp_filesystem->get_contents($api);
    }
    if($count_linkedin){
        $count = json_decode( $count_linkedin );
        return  $count->count;
    }
    return false;
}

function pinterest_count( $url ) {
    $check_url = 'http://api.pinterest.com/v1/urls/count.json?callback=pin&url=' . urlencode( $url );
    $response = wp_remote_retrieve_body( wp_remote_get( $check_url ) );

    $response = str_replace( 'pin({', '{', $response);
    $response = str_replace( '})', '}', $response);
    $encoded_response = json_decode( $response, true );

    $share_count = intval( $encoded_response['count'] );
    return $share_count;
}

function google_count( $url ) {
    if( !$url ) {
        return 0;
    }
    if ( !filter_var($url, FILTER_VALIDATE_URL) ){
        return 0;
    }
    foreach (array('apis', 'plusone') as $host) {
        $ch = curl_init(sprintf('https://%s.google.com/u/0/_/+1/fastbutton?url=%s',
            $host, urlencode($url)));
        curl_setopt_array($ch, array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_USERAGENT      => 'Mozilla/5.0 (Windows NT 6.1; WOW64) ' .
                'AppleWebKit/537.36 (KHTML, like Gecko) ' .
                'Chrome/32.0.1700.72 Safari/537.36' ));
        $response = curl_exec($ch);
        $curlinfo = curl_getinfo($ch);
        curl_close($ch);
        if (200 === $curlinfo['http_code'] && 0 < strlen($response)) { break 1; }
        $response = 0;
    }

    if( !$response ) {
        return 0;
    }
    preg_match_all('/window\.__SSR\s\=\s\{c:\s(\d+?)\./', $response, $match, PREG_SET_ORDER);
    return (1 === sizeof($match) && 2 === sizeof($match[0])) ? intval($match[0][1]) : 0;
}

//load_Shop ============================================================================================================
add_action('wp_ajax_load_more_shop', 'nano_load_more_shop');
add_action('wp_ajax_nopriv_load_more_shop', 'nano_load_more_shop');
function nano_load_more_shop(){

    $cat                    = $_POST['cat'];
    $pages                  = $_POST['pages'];
    $col                    = $_POST['col'];
    $paged                  = $_POST['paged'];

    $args = array(
        'post_type'           => 'product',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => true,
        'posts_per_page'      => $pages,
        'product_cat'         => $cat,
        'paged'               => $paged,

    );
    $the_query = new WP_Query( $args );

        if ($the_query->have_posts()):
            while ($the_query->have_posts()): $the_query->the_post(); ?>
                <?php {?>
                    <li <?php post_class('col-item col-md-4'); ?>>
                        asdsadasd;
                    </li>
                <?php }
                endwhile;
        endif;
        wp_reset_postdata();
        wp_die();

    //end
}

//load_product ============================================================================================================
add_action('wp_ajax_load_more_products', 'nano_load_more_products');
add_action('wp_ajax_nopriv_load_more_products', 'nano_load_more_products');
function nano_load_more_products(){

    $cat                    = $_POST['cat'];
    $number                 = $_POST['number'];
    $paged                  = $_POST['paged'];
    $layout                 = $_POST['layout'];

    $args = array(
        'post_type'           => 'product',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => true,
        'posts_per_page'      => $number,
        'product_cat'         => $cat,
        'paged'               => $paged,

    );
    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()):
        while ($the_query->have_posts()): $the_query->the_post(); ?>
            <?php {?>
                <li <?php post_class('col-item'); ?>>
                    <?php wc_get_template_part( 'layouts/content-product-'.$layout);?>
                </li>
            <?php }
        endwhile;
    endif;
    wp_reset_postdata();
    wp_die();

    //end
}

//load_more_post
add_action('wp_ajax_load_more_post', 'nano_load_more_post');
add_action('wp_ajax_nopriv_load_more_post', 'nano_load_more_post');
function nano_load_more_post(){

    $cat            = $_POST['cat'];
    $number         = $_POST['number'];
    $col            = $_POST['col'];
    $paged          = $_POST['paged'];
    $ads            = $_POST['ads'];
    $layout         = $_POST['layout'];

    $args = array(
        'post_type'           => 'post',
        'orderby'             => 'date',
        'order'               => 'DESC',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => true,
        'posts_per_page'      => $number,
        'category_name'       => $cat,
        'paged'               => $paged,
    );

    $the_query = new WP_Query( $args );
    $i=1;
    if($layout == 'grid'){
        if ($the_query->have_posts()):
            while ($the_query->have_posts()): $the_query->the_post(); ?>

                <?php if($i == 1 && $ads =='leaderboard'){?>
                    <div class="col-item ads-item col-xs-12 col-sm-12 col-md-12 center" >
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_leaderboard'));?>
                    </div>
                <?php }?>
                <div class="col-item <?php echo esc_attr($col);?>">
                    <?php na_part_templates('layout/content-grid');?>
                </div>
                <?php if($i == $number && $ads =='large-rectangle'){?>
                    <div class="col-item ads-item <?php echo esc_attr($col);?>">
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_rectangle'));?>
                    </div>
                <?php }
                 $i++; endwhile;
        endif;
        wp_reset_postdata();
        wp_die();
        ?>
    <?php } elseif($layout == 'list'){?>
        <?php if ($the_query->have_posts()):
            while ($the_query->have_posts()): $the_query->the_post(); ?>

                <?php if($i == 1 && $ads =='leaderboard'){?>
                    <div class="col-item ads-item col-xs-12 col-sm-12 col-md-12 center">
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_leaderboard'));?>
                    </div>
                <?php }?>
                <div class="col-item <?php echo esc_attr($col);?>">
                    <?php na_part_templates('layout/content-list');?>
                </div>
                <?php if($i == $number && $ads =='large-rectangle'){?>
                    <div class="col-item ads-item <?php echo esc_attr($col);?>">
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_rectangle'));?>
                    </div>
                <?php }?>
                <?php $i++; endwhile;
        endif;
        wp_reset_postdata();
        wp_die();
        ?>
    <?php }
    elseif($layout == 'tran'){?>
        <?php if ($the_query->have_posts()):
            while ($the_query->have_posts()): $the_query->the_post(); ?>

                <?php if($i == 1 && $ads =='leaderboard'){?>
                    <div class="col-item ads-item col-xs-12 col-sm-12 col-md-12 center">
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_leaderboard'));?>
                    </div>
                <?php }?>
                <div class="col-item <?php echo esc_attr($col);?>">
                    <?php na_part_templates('layout/content-tran');?>
                </div>
                <?php if($i == $number && $ads =='large-rectangle'){?>
                    <div class="col-item ads-item <?php echo esc_attr($col);?>">
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_rectangle'));?>
                    </div>
                <?php }?>

                <?php $i++; endwhile;
        endif;
        wp_reset_postdata();
        wp_die();
    } else{
        if ($the_query->have_posts()):
            while ($the_query->have_posts()): $the_query->the_post(); ?>

                <?php if($i == 1 && $ads =='leaderboard'){?>
                    <div class="col-item ads-item col-xs-12 col-sm-12 col-md-12 center">
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_leaderboard'));?>
                    </div>
                <?php }?>

                <?php if($i%2==0){?>
                    <div class="col-item <?php echo esc_attr($col);?>">
                        <?php na_part_templates('layout/content-tran');?>
                    </div>
                <?php } else{?>
                    <div class="col-item <?php echo esc_attr($col);?>">
                        <?php na_part_templates('layout/content-grid');?>
                    </div>
                <?php } ?>

                <?php if($i == $number && $ads =='large-rectangle'){?>
                    <div class="col-item ads-item <?php echo esc_attr($col);?>">
                        <?php echo wp_kses_post(get_theme_mod('trebol_ads_rectangle'));?>
                    </div>
                <?php }
                $i++; endwhile;
        endif;
        wp_reset_postdata();
        wp_die();
    }
    //end
}

//load more category
add_action('wp_ajax_load_more_category', 'nano_load_more_category');
add_action('wp_ajax_nopriv_load_more_category', 'nano_load_more_category');
function nano_load_more_category(){
    $cat            = $_POST['cat'];
    $number         = $_POST['number'];
    $col            = $_POST['col'];
    $ads            = $_POST['ads'];
    $layout         = $_POST['layout'];

    $args = array(
        'post_type'           => 'post',
        'orderby'             => 'date',
        'order'               => 'DESC',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => true,
        'posts_per_page'      => $number,
        'category_name'       => $cat,
    );
    $the_query = new WP_Query( $args );
    $num_pages = $the_query->max_num_pages;
    $i=1;
    ?>
    <div id="<?php echo esc_attr($cat);?>" class="archive-blog row  affect-isotope active">
        <span id="filterPages" class="hidden" data-filter-number="<?php echo esc_attr($number);?>" data-filter-cat="<?php echo esc_attr($cat);?>" data-filter-pages="<?php echo esc_attr($num_pages);?>"></span>
        <?php
        if($layout == 'grid'){
            if ($the_query->have_posts()):
                while ($the_query->have_posts()): $the_query->the_post(); ?>

                    <div class="col-item <?php echo esc_attr($col);?>">
                        <?php na_part_templates('layout/content-grid');?>
                    </div>

                    <?php
                    $i++; endwhile;
            endif;
            wp_reset_postdata();
            wp_die();
        }
        if($layout == 'list'){
            if ($the_query->have_posts()):
                while ($the_query->have_posts()): $the_query->the_post(); ?>

                    <div class="col-item <?php echo esc_attr($col);?>">
                        <?php na_part_templates('layout/content-list');?>
                    </div>

                    <?php
                    $i++; endwhile;
            endif;
            wp_reset_postdata();
            wp_die();
        }?>

    </div>
    <?php
    //end
}


if( ! function_exists( 'nano_pagination' ) ) {
    function nano_pagination(  $range = 2, $current_query = '', $pages = '' ) {
        $showitems = ($range * 2)+1;
        $add_class='paging_numbers';
        $typePagination  = get_theme_mod('trebol_woo_pagination',false);
        if($typePagination){
            $add_class='paging_loadMore';
        }
        if( $current_query == '' ) {
            global $paged;
            if( empty( $paged ) ) $paged = 1;
        } else {
            $paged = $current_query->query_vars['paged'];
        }

        if( $pages == '' ) {
            if( $current_query == '' ) {
                global $wp_query;
                $pages = $wp_query->max_num_pages;
                if(!$pages) {
                    $pages = 1;
                }
            } else {
                $pages = $current_query->max_num_pages;
            }
        }

        if(1 != $pages) { ?>
            <nav class="paging <?php echo esc_attr($add_class);?>" data-pages="<?php echo esc_attr($pages);?>">
                <ul class="pagination">
                    <?php if ( $paged > 1 ) { ?>
                        <li>
                            <a class="prev page-numbers" href="<?php echo esc_url(get_pagenum_link($paged - 1)) ?>"><i class="fa fa-angle-left"></i></a>
                        </li>
                    <?php }

                    for ($i=1; $i <= $pages; $i++) {
                        if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
                            if ($paged == $i) { ?>
                                <li>
                                    <span class="page-numbers current"><?php echo esc_html($i) ?></span>
                                </li>
                            <?php } else { ?>
                                <li>
                                    <a href="<?php echo esc_url(get_pagenum_link($i)) ?>" class="inactive page-numbers"><?php echo esc_html($i) ?></a>
                                </li>
                                <?php
                            }
                        }
                    }
                    if ($paged < $pages) { ?>
                        <li>
                            <a class="next page-numbers" href="<?php echo esc_url(get_pagenum_link($paged + 1)) ?>"><i class="fa fa-angle-right"></i></a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
            <div class="infload-link"><a class="next-page" href="<?php echo esc_url(get_pagenum_link($paged + 1)) ?>"></a></div>
            <?php if($typePagination):?>
                <div class="infload-controls">
                    <a href="#" id="loadShop" class="infload-btn"><?php esc_html_e( 'load more', 'trebol' ); ?></a>
                    <a href="#" class="infload-to-top hidden"><?php esc_html_e( 'All products loaded.', 'trebol' ); ?></a>
                </div>
            <?php endif;?>
            <?php
        }
    }
}


if( !function_exists( 'nano_get_query_var' ) ) {
    function nano_get_query_var( $var, $default = null){
        if((is_front_page() || is_home()) && $var == 'paged'){
            $var = 'page';
        }
        return  get_query_var( $var, $default );
    }
}

function getCSSAnimation( $css_animation ) {
    $output = '';
    if ( '' !== $css_animation && 'none' !== $css_animation ) {
        wp_enqueue_script( 'waypoints' );
        wp_enqueue_style( 'animate-css' );
        $output = ' wpb_animate_when_almost_visible wpb_' . $css_animation . ' ' . $css_animation;
    }

    return $output;
}
/* VIDEO ======================================================================================*/
function nano_load_videos(){
    $id            = $_POST['id'];
    ?>
    <div class="embed-responsive  embed-responsive-16by9 video-responsive post-video single-video embed-responsive embed-responsive-16by9">
        <?php $sp_video = get_post_meta( $id, '_format_video_embed', true ); ?>
        <?php if(wp_oembed_get( $sp_video)) {
            $idVideo='video'.$id;
            echo nano_oembed_get($sp_video, 0,$idVideo);
        } ?>
    </div>

    <?php
    //end
}
function nano_oembed_get( $url, $autoplay,$idVideo,$args = '') {
    // Manually build the IFRAME embed with the related videos option disabled and autoplay turned on
    if(preg_match("/youtube.com\/watch\?v=([^&]+)/i", $url, $aMatch)){
        return '<iframe  class="video-item " id='.$idVideo.' width="1170" height="657" src="http://www.youtube.com/embed/' . $aMatch[1] . '?enablejsapi=1&rel=0&autoplay='.$autoplay.'&showinfo=0"  volume="0" frameborder="0" allowfullscreen></iframe>';
    }

    if(preg_match("/vimeo\.com\/(\w+\s*\/?)*([0-9]+)*$/i", $url, $aMatch)){
        return '<iframe class="video-item " id='.$idVideo.' width="1170" height="657" src="https://player.vimeo.com/video/' . $aMatch[1] . '?enablejsapi=1&rel=0&autoplay='.$autoplay.'&showinfo=0"  volume="0" frameborder="0" allowfullscreen></iframe>';
    }

    require_once( ABSPATH . WPINC . '/class-oembed.php' );
    $oembed = _wp_oembed_get_object();
    return $oembed->get_html( $url, $args );
}
/* Facebook Comments =================================================================================================*/
add_action('wp_head', 'trebol_facebook_comments');
function trebol_facebook_comments() {
    $app_id=get_theme_mod('trebol_comments_single',''); ?>
    <meta property="fb:app_id" content="<?php echo esc_attr($app_id);?>" />
<?php }
// WooCommerce - Add Wishlist to after Addtocart =======================================================================
add_action('woocommerce_before_single_product_summary', 'trebol_wishlist_button', 31);
// WooCommerce - YITH WCWL Wishlist ====================================================================================
add_action( 'trebol_yith_wishlist', 'trebol_wishlist_button');
if (!function_exists('trebol_wishlist_button')) {
    function trebol_wishlist_button() {?>
        <div class="wishlist-buttom">
            <?php if (in_array('yith-woocommerce-wishlist/init.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ))) {
                echo do_shortcode('[yith_wcwl_add_to_wishlist]');
            }?>
        </div>
        <?php
    }
}