<?php

if (!function_exists('nano_shortcode_feature_services')) {
    function nano_shortcode_feature_services($atts){    
        $atts = shortcode_atts(array(
            'css'      => '',
            'style'    => 'style1',
            'items'    => '',
            'el_class' => ''
        ), $atts);
        ob_start();
        nano_template_part('shortcode', 'feature-services', array('atts' => $atts));
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_feature_services', 'nano_shortcode_feature_services');

add_action('vc_before_init', 'nano_feature_services_integrate_vc');

if (!function_exists('nano_feature_services_integrate_vc')) {
    function nano_feature_services_integrate_vc(){
        vc_map(array(
            'name'        => esc_html__('NA Featured Services', 'nano'),
            'base'        => 'na_feature_services',
            'category'    => esc_html__('NA', 'nano'),
            'icon'        => 'nano-feature-services',
            'description' => esc_html__('Creating a or multiple selection on custom services', 'nano'),
            "params"      => array(  
                array(
                    "type"       => "dropdown",
                    "heading"    => esc_html__("Layout Styles", 'nano'),
                    "param_name" => "style",
                    'value'      => array(
                        esc_html__('Style 1', 'nano') => 'style1',
                        esc_html__('Style 2', 'nano') => 'style2',
                        esc_html__('Style 3', 'nano') => 'style3',                        
                    ),
                    'std'        => 'style1',
                    'group'      => esc_html__( 'Settings', 'nano' ),
                ), 
                array(
                    'type'       => 'param_group',
                    'heading'    => esc_html__('Custom Services Profiles', 'nano' ),
                    'param_name' => 'items',
                    'group'      => esc_html__( 'Services Profiles', 'nano' ),
                    'params'     => array(
                        array(
                            "type"        => "attach_image",                            
                            "param_name"  => "image_box",
                            "value"       => '',                            
                            'heading'     => esc_html__('Icon Image', 'nano' ),
                            "description" => esc_html__("Upload an icon image.", 'nano'),
                        ),
                        array(
                            "type"        => "textfield",
                            "class"       => "",                            
                            "param_name"  => "title",
                            'admin_label' => true,
                            "heading"     => esc_html__('Title','nano'),
                            "description" => esc_html__('Enter a title','nano'),
                        ),
                        array(
                            "type"        => "textarea",
                            "class"       => "",                                                        
                            "param_name"  => "description",                 
                            "heading"     => esc_html__('Description','nano'),               
                            "description" => esc_html__('Enter a description','nano'),
                        ), 
                    ),
                ),                             
                array(
                    'type'        => 'textfield',                    
                    'param_name'  => 'el_class',
                    'heading'     => esc_html__( 'Extra class name', 'nano' ),
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    'type'       => 'css_editor',
                    'heading'    => esc_html__( 'Css', 'nano' ),
                    'param_name' => 'css',
                    'group'      => esc_html__( 'Design options', 'nano' ),
                )                
            )
        ));
    }
}