<?php

/* ================================================================================== */
/* Information About Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_brand')) {
    function na_shortcode_brand($atts, $content) {
        $outputs='';
        $atts = shortcode_atts(array(            
            'items'    => '',                           
            'el_class' => '',                  
            'css'      => '',        
        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'brand' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_brand', 'na_shortcode_brand');

add_action('vc_before_init', 'na_brand_integrate_vc');

if (!function_exists('na_brand_integrate_vc')) {
    function na_brand_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Brand Images', 'nano'),
                'base' => 'na_brand',      
                'icon'     => 'nano-brand',          
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show list images (logo) of brand', 'nano'),
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Brand Logo Images', 'nano' ),
                        'param_name' => 'items',                        
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("Upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                "description" => esc_html__( "Enter a image or logo for Brand", "nano" )
                            ),                                                        
                            array(
                                'type' => 'vc_link',
                                'heading' => esc_html__( 'Link', 'nano' ),
                                'param_name' => 'url',                                
                                "description" => esc_html__( "Enter a link to partner", "nano" )
                            )                          
                        )
                    ),                                    
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' )
                    ),                    
                    array(
                        'type'       => 'css_editor',
                        'heading'    => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group'      => esc_html__( 'Design options', 'nano' ),
                    )
                )
            )
        );
    }
}