<?php

/* ================================================================================== */
/*      Products Shortcode
/* ================================================================================== */
if (!function_exists('na_shortcode_products')) {
    function na_shortcode_products($atts, $content) {
        $atts = shortcode_atts(array(
            'box_title'             => '',
            'post_type'             => 'product',
            'column'                => '4',
            'number'                => 8,
            'meta_query'            => '',
            'tax_query'             => array(),
            'list_type'             => 'latest',
            'products_types'        => 'grid',
            'box_layouts'           => 'grid',
            'ignore_sticky_posts'   => 1,
            'show'                  => '',
            'style'                 => 'default',
            'category'              => '',
            'slider_dots'           => '',                     
            'padding_bottom_module' => '0px',
            'url' => '',            
            'el_class'              => '',      
            'css'                   => '',            
        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'products' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_products', 'na_shortcode_products');

add_action('vc_before_init', 'na_products_integrate_vc');

if (!function_exists('na_products_integrate_vc')) {
    function na_products_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Block Products', 'nano'),
                'base' => 'na_products',
                'icon' => 'nano-products',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Block products .', 'nano'),
                'params' => array(
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__('Title', 'nano'),
                        'value'      => '',
                        'param_name' => 'box_title',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Show product type', 'nano'),
                        'param_name' => 'list_type',
                        'value' => array(
                            esc_html__('Latest Products', 'nano') => 'latest',
                            esc_html__('Featured Products', 'nano') => 'featured',
                            esc_html__('Best Selling Products', 'nano') => 'best-selling',
                            esc_html__('TopRated Products', 'nano') => 'toprate',
                            esc_html__('Special Products', 'nano') => 'onsale'
                        ),
                        'std' => 'latest',
                    ),
                    array(
                        'type' => 'nano_product_categories',
                        'heading' => esc_html__( 'Category','nano' ),
                        'param_name' => 'category',
                        'description' => esc_html__( 'Product category list','nano'),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Block layout', 'nano'),
                        'value' => array(
                            esc_html__('Grid', 'nano') => 'grid',
                            esc_html__('Carousel', 'nano') => 'carousel',
                        ),
                        'std' => 'grid',
                        'param_name' => 'box_layouts',
                    ),                    
                    array(
                        'type' => 'nano_image_radio',
                        'heading' => esc_html__('Layout Product', 'nano'),
                        'value' => array(
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-tran.jpg', 'nano')        => 'trans',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-grid.jpg', 'nano')        => 'grid',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-gallery.jpg', 'nano')     => 'gallery',
                        ),
                        'width' => '100px',
                        'height' => '70px',
                        'param_name' => 'products_types',
                        'std' => 'grid',
                        'description' => esc_html__('Select layout type', 'nano'),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Products per row', 'nano'),
                        'value' => array(
                            esc_html__('2', 'nano') => '2',
                            esc_html__('3', 'nano') => '3',
                            esc_html__('4', 'nano') => '4',
                            esc_html__('5', 'nano') => '5',
                            esc_html__('6', 'nano') => '6'
                        ),
                        'std' => '4',
                        'param_name' => 'column',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => esc_html__( 'Show Slider Dots', 'nano' ),
                        'param_name' => 'slider_dots',                        
                        'std' => '',                  
                        'description' => esc_html__( 'Compatible with product layout carousel slider', 'nano' )                               
                    ),                    
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Number of Products show on the block', 'nano'),
                        'value' => 8,
                        'param_name' => 'number',
                    ),
                    array(
                        'type'       => 'vc_link',
                        'heading'    => esc_html__( 'Button', 'nano' ),
                        'param_name' => 'url',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' )
                    ),
                    array(
                        'type'       => 'css_editor',
                        'heading'    => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group'      => esc_html__( 'Design options', 'nano' ),
                    )
                )
            )
        );
    }
}