<?php

/* ================================================================================== */
/* Team Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_team')) {
    function na_shortcode_team($atts, $content) {
        $atts = shortcode_atts(
            array(                                            
                'el_class' => '',
                'css'      => '',
                'items'    => '',
            ), $atts);

        ob_start();            
            nano_template_part('shortcode', 'team' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_team', 'na_shortcode_team');

add_action('vc_before_init', 'na_team_integrate_vc');

if (!function_exists('na_team_integrate_vc')) {
    function na_team_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Team Members', 'nano'),
                'base' => 'na_team',                
                'icon'     => 'nano-team',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Created a list of team members', 'nano'),       
                'params' => array( 
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' ),
                        'group' => esc_html__( 'Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Team Member Item', 'nano' ),
                        'param_name' => 'items',
                        'group' => esc_html__( 'Profiles', 'nano' ),
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("Upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'   => esc_html__('Image', 'nano' ),
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title','nano'),
                                "param_name" => "title",
                                'admin_label' => true,
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Job Titles','nano'),
                                "param_name" => "job_title",
                                'admin_label' => true,
                            )                                                      
                        )
                    ),                     
                    array(
                        'type' => 'css_editor',
                        'heading' => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => esc_html__( 'Design options', 'nano' ),
                    )
                )
            )
        );
    }
}