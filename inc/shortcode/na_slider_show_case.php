<?php

/* ================================================================================== */
/* Information About Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_slide_show_case')) {
    function na_shortcode_slide_show_case($atts, $content) {
        $outputs='';
        $atts = shortcode_atts(array(
            'block_title'       => '',
            'items'             => '',
            'link_facebook'     => '#',
            'link_twitter'      => '#',
            'link_google'       => '',
            'link_linkedin'     => '',
            'link_instagram'    => '#',
            'link_dribbble'     => '',
            'link_pinterest'    => '',
        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'slider-showCase' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('slide_show_case', 'na_shortcode_slide_show_case');

add_action('vc_before_init', 'na_slide_show_case_integrate_vc');

if (!function_exists('na_slide_show_case_integrate_vc')) {
    function na_slide_show_case_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA:Slide Show Case', 'nano'),
                'base' => 'slide_show_case',
                'icon'     => 'nano-slide_show_case',          
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Tabs About', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'param_name' => 'block_title',
                        'heading' => esc_html__( 'Widget title', 'js_composer' ),
                        'description' => esc_html__( 'Enter text used as widget title.', 'js_composer' ),
                    ),

                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Section Tabs', 'nano' ),
                        'param_name' => 'items',
                        'group' => esc_html__( 'Slider Group', 'nano' ),
                        'params' => array(
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title Section','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title Sub Section','nano'),
                                "param_name" => "sub_box",
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__( 'Link Section', 'nano' ),
                                'param_name' => 'link_box',
                                "value"       => '#',
                            ),
                            array(
                                'type' => 'colorpicker',
                                'heading' => esc_html__( 'Background color', 'nano' ),
                                'param_name' => 'bg_box',
                                'description' => esc_html__( 'Select custom background color.', 'nano' ),
                            ),
                            array(
                                 'type' => 'param_group',
                                 'heading' => esc_html__('Slider Group', 'nano' ),
                                 'param_name' => 'slider_box',
                                 'group' => esc_html__( 'Slider Group', 'nano' ),
                                 'params' => array(
                                     array(
                                         "type" => "attach_image",
                                         "description" => esc_html__("Upload an image.", 'nano'),
                                         "param_name" => "image_slider",
                                         "value" => '',
                                         'heading'   => esc_html__('Image', 'nano' ),
                                     ),
                                     array(
                                         "type" => "textfield",
                                         "class" => "",
                                         "heading" => esc_html__('Title ','nano'),
                                         "param_name" => "title_slider",
                                         'admin_label' => true,
                                     ),
                                     array(
                                         'type' => 'vc_link',
                                         'heading' => esc_html__( 'Button', 'nano' ),
                                         'param_name' => 'link_slider',
                                     )
                                 ),
                             ),
                        )
                    ),

                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Facebook', 'nano' ),
                        'param_name' => 'link_facebook',
                        'value'       => '#',
                        'group' => esc_html__( 'Share', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Twitter', 'nano' ),
                        'param_name' => 'link_twitter',
                        'value'       => '#',
                        'group' => esc_html__( 'Share', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Google Plus', 'nano' ),
                        'param_name' => 'link_google',
                        'value'       => '',
                        'group' => esc_html__( 'Share', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'LinkedIn', 'nano' ),
                        'param_name' => 'link_linkedin',
                        'value'       => '',
                        'group' => esc_html__( 'Share', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Pinterest', 'nano' ),
                        'param_name' => 'link_pinterest',
                        'value'       => '',
                        'group' => esc_html__( 'Share', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Instagram', 'nano' ),
                        'param_name' => 'link_instagram',
                        'value'       => '#',
                        'group' => esc_html__( 'Share', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Dribbble', 'nano' ),
                        'param_name' => 'link_dribbble',
                        'value'       => '',
                        'group' => esc_html__( 'Share', 'nano' ),
                    )
                ),
      ));
    }
}