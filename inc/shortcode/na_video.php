<?php

if (!function_exists('nano_shortcode_video')) {
    function nano_shortcode_video($atts){    
        $atts = shortcode_atts(array(
            'title'       => '',            
            'align'       => 'text-left',
            'style'       => 'style1',
            'description' => '',
            'image_box'   => '',                        
            'url'         => '',
            'video_link'  => '',            
            'el_class'    => '',
            'css'         => '',        
        ), $atts);
        ob_start();
        nano_template_part('shortcode', 'video', array('atts' => $atts));
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_video', 'nano_shortcode_video');

add_action('vc_before_init', 'nano_video_integrate_vc');
if (!function_exists('nano_video_integrate_vc')) {
    function nano_video_integrate_vc(){
        vc_map(array(
            'name'     => esc_html__('NA Video', 'nano'),
            'base'     => 'na_video',
            'category' => esc_html__('NA', 'nano'),
            'icon'     => 'nano-video',
            'description' => esc_html__('Displays a video with content', 'nano'),
            "params"   => array(                
                array(
                    "type"        => "textarea",
                    "class"       => "",
                    "heading"     => esc_html__("Title", 'nano'),
                    "param_name"  => "title",
                    'admin_label' => true,
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__( 'Title Align', 'nano' ),
                    'param_name' => 'align',
                    'value'      => array(
                        esc_html__( 'Center', 'nano' ) => 'text-center',
                        esc_html__( 'Left', 'nano' )   => 'text-left',
                        esc_html__( 'Right', 'nano' )  => 'text-right'
                    ),
                    'std'   => 'text-left',
                    'group' => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    "type"        => "textarea_html",
                    "holder"      => "div",
                    "class"       => "",
                    'heading'     => esc_html__( 'Description', 'nano' ),
                    "param_name"  => "description",
                    "value"       => '',
                    'description' => esc_html__( 'Enter description for title.', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    "type"        => "attach_image",
                    "description" => esc_html__("Upload an image.", 'nano'),
                    "param_name"  => "image_box",
                    "value"       => '',
                    'heading'     => esc_html__('Image', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    'type'       => 'vc_link',
                    'heading'    => esc_html__( 'Button', 'nano' ),
                    'param_name' => 'url',
                    'group'      => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    'type'       => 'vc_link',
                    'heading'    => esc_html__( 'Link Video', 'nano' ),
                    'param_name' => 'video_link',
                    'group'      => esc_html__( 'Settings', 'nano' ),
                ),        
                array(
                    'type'    => 'dropdown',
                    'heading' => esc_html__('Layout Style', 'nano'),
                    'value'   => array(
                        esc_html__('Layout Style 1', 'nano')  => 'style1',
                        esc_html__('Layout Style 2', 'nano')  => 'style2',                        
                        esc_html__('Layout Style 3', 'nano')  => 'style3',                        
                    ),
                    'std'        => 'style1',                        
                    'param_name' => 'style', 
                    'group'      => esc_html__( 'Settings', 'nano' ),                       
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'nano' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    'type'       => 'css_editor',
                    'heading'    => esc_html__( 'Css', 'nano' ),
                    'param_name' => 'css',
                    'group'      => esc_html__( 'Design options', 'nano' ),
                )
            )
        ));
    }
}
