<?php
if (!function_exists('na_shortcode_testimonial')) {
    function na_shortcode_testimonial($atts,$output)
    {
        $atts = shortcode_atts(
            array(                                
                'title' => '',
                'css'   => '',
                'style' => 'style1',
                'items' => '',
            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'testimonial' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_testimonial', 'na_shortcode_testimonial');

add_action('vc_before_init', 'na_testimonial_integrate_vc');

if (!function_exists('na_testimonial_integrate_vc')) {
    function na_testimonial_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Testimonials', 'nano'),
                'base' => 'na_testimonial',   
                'icon' => 'nano-testimonial',             
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Create a block or multi block testimonial with data content', 'nano'),
                'params' => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Title','nano'),
                        "param_name" => "title",                        
                        'admin_label' => true,
                    ),
                    array(
                        "type"       => "dropdown",
                        "heading"    => esc_html__("Style", 'nano'),
                        "param_name" => "style",
                        'value'      => array(
                            esc_html__('Style 1', 'nano') => 'style1',
                            esc_html__('Style 2', 'nano') => 'style2',                            
                            esc_html__('Style 3', 'nano') => 'style3',                            
                        ),
                        'std'        => 'style1'
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Profile Testimonials', 'nano' ),
                        'param_name' => 'items',
                        'group' => esc_html__( 'Profiles', 'nano' ),
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('Image Profile', 'nano' ),
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Name','nano'),
                                "param_name" => "name",
                                'admin_label' => true,
                            ),
                            array(
                                "type" => "exploded_textarea",
                                "class" => "",
                                "heading" => esc_html__('List of Jobs or Job Titles','nano'),
                                "param_name" => "job_titles",                                
                            ),                         
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__('Quotes','nano'),
                                "param_name" => "quotes",
                            ), 
                        ),
                    ),                    
                    array(
                        'type' => 'css_editor',
                        'heading' => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => esc_html__( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}
