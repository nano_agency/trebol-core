<?php

/* ================================================================================== */
/* Category Banners Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_category_slider')) {
    function na_shortcode_category_slider($atts, $content) {
        $atts = shortcode_atts(
            array(                            
                'image_box' => '',
                'title'     => '',
                'subtitle'  => '',                
                'name'      => '',                
                'url'       => '',
                'el_class'  => '',
                'css'       => '',              
            ), $atts);
        ob_start();            
            nano_template_part('shortcode', 'category-slider' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_category_slider', 'na_shortcode_category_slider');

add_action('vc_before_init', 'na_category_slider_integrate_vc');

if (!function_exists('na_category_slider_integrate_vc')) {
    function na_category_slider_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Category Slider', 'nano'),
                'base' => 'na_category_slider',                
                'icon'     => 'nano-category-banners',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Create collection display in a slider banner', 'nano'),   
                'params' => array(                     
                    array(
                        "type"        => "attach_images",
                        'heading'     => esc_html__('Image slider', 'nano' ),
                        "description" => esc_html__("Upload multi image.", 'nano'),
                        "param_name"  => "image_box",
                        "value"       => "",
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__('Title','nano'),
                        "param_name"  => "title",
                        'admin_label' => true,
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__('Sub Title','nano'),
                        "param_name"  => "subtitle",
                        'admin_label' => true,
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Define Name', 'nano' ),
                        'param_name'  => 'name',
                        'description' => esc_html__( 'Define alias name.', 'nano' ),                        
                    ),
                    array(
                        'type'       => 'vc_link',
                        'heading'    => esc_html__( 'Button', 'nano' ),
                        'param_name' => 'url',
                    ),   
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' ),                        
                    ),             
                    array(
                        'type'       => 'css_editor',
                        'heading'    => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group'      => esc_html__( 'Design options', 'nano' ),
                    )
                )
            )
        );
    }
}