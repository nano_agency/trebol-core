<?php

/* ================================================================================== */
/*      Category Colections Shortcode
/* ================================================================================== */
if (!function_exists('na_shortcode_category_colections')) {
    function na_shortcode_category_colections($atts, $content) {
        $atts = shortcode_atts(array(                  
            'image_box'             => 'left',
            'title_box'             => '',
            'url'                   => '',
            'position_layouts'      => 'left',
            'post_type'             => 'product',
            'column'                => '4',
            'number'                => 8,
            'meta_query'            => '',
            'tax_query'             => array(),
            'list_type'             => 'latest',
            'products_types'        => 'gallery',
            'box_layouts'           => 'carousel',
            'ignore_sticky_posts'   => 1,
            'show'                  => '',
            'style'                 => 'default',
            'category'              => '',
            'slider_dots'           => '',
            'padding_bottom_module' => '0px',
        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'category-colections' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_category_colections', 'na_shortcode_category_colections');

add_action('vc_before_init', 'na_category_colections_integrate_vc');

if (!function_exists('na_category_colections_integrate_vc')) {
    function na_category_colections_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Category Colections', 'nano'),
                'base' => 'na_category_colections',                
                'icon'     => 'nano-category-colections',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show a block Featured Collections include: banner and list products', 'nano'),   
                'params' => array(       
                    array(
                        "type" => "attach_image",
                        "description" => esc_html__("upload an image.", 'nano'),
                        "param_name" => "image_box",
                        "value" => '',
                        'heading'   => esc_html__('Category Image', 'nano' ),
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__('Title','nano'),
                        "param_name"  => "title_box",
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => esc_html__( 'Button', 'nano' ),
                        'param_name' => 'url',
                    ),     
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Position Layouts', 'nano'),
                        'value' => array(
                            esc_html__('Left', 'nano') => 'left',
                            esc_html__('Right', 'nano') => 'right',
                        ),
                        'std' => 'left',
                        'param_name' => 'position_layouts',                        
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Show product type', 'nano'),
                        'param_name' => 'list_type',
                        'value' => array(
                            esc_html__('Latest Products', 'nano') => 'latest',
                            esc_html__('Featured Products', 'nano') => 'featured',
                            esc_html__('Best Selling Products', 'nano') => 'best-selling',
                            esc_html__('TopRated Products', 'nano') => 'toprate',
                            esc_html__('Special Products', 'nano') => 'onsale'
                        ),
                        'std' => 'latest',
                        'group' => esc_html__( 'List Product Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'nano_product_categories',
                        'heading' => esc_html__( 'Category','nano' ),
                        'param_name' => 'category',
                        'description' => esc_html__( 'Product category list','nano'),
                        'group' => esc_html__( 'List Product Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Block layout', 'nano'),
                        'value' => array(                            
                            esc_html__('Carousel', 'nano') => 'carousel',
                        ),
                        'std' => 'carousel',
                        'param_name' => 'box_layouts',
                        'group' => esc_html__( 'List Product Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'nano_image_radio',
                        'heading' => esc_html__('Layout Product', 'nano'),
                        'value' => array(
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-tran.jpg', 'nano')        => 'gallery',                          
                        ),
                        'width' => '100px',
                        'height' => '70px',
                        'param_name' => 'products_types',
                        'std' => 'gallery',
                        'description' => esc_html__('Select layout type', 'nano'),
                        'group' => esc_html__( 'List Product Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Products per row', 'nano'),
                        'value' => array(
                            esc_html__('2', 'nano') => '2',
                            esc_html__('3', 'nano') => '3',
                            esc_html__('4', 'nano') => '4',
                            esc_html__('5', 'nano') => '5',
                            esc_html__('6', 'nano') => '6'
                        ),
                        'std' => '4',
                        'param_name' => 'column',
                        'group' => esc_html__( 'List Product Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => esc_html__( 'Show Slider Dots', 'nano' ),
                        'param_name' => 'slider_dots',
                        'std' => '',                         
                        'group' => esc_html__( 'List Product Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Number of Products show on the block', 'nano'),
                        'value' => 8,
                        'param_name' => 'number',
                        'group' => esc_html__( 'List Product Settings', 'nano' ),
                    ),
                )
            )
        );
    }
}