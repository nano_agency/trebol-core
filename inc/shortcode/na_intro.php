<?php

/* ================================================================================== */
/* Information About Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_intro')) {
    function na_shortcode_intro($atts, $content) {
        $outputs='';
        $atts = shortcode_atts(array(
            'title'       => '',      
            'images'      => '',                  
            'description' => '',                  
            'title_align' => 'text-left',                  
            'url'         => '',                  
            'el_class'    => '',                  
            'css'         => '',        
        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'intro' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_intro', 'na_shortcode_intro');

add_action('vc_before_init', 'na_intro_integrate_vc');

if (!function_exists('na_intro_integrate_vc')) {
    function na_intro_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Intro', 'nano'),
                'base' => 'na_intro',      
                'icon'     => 'nano-intro',          
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Information About', 'nano'),
                'params' => array(
                    array(
                        "type"        => "attach_images",
                        "class"       => "",
                        "heading"     => esc_html__( "Images", "nano" ),
                        "param_name"  => "images",
                        "value"       => '',
                        "description" => esc_html__( "Enter Images Gallery", "nano" )
                    ),                    
                    array(
                        'type'       => 'textarea',
                        'heading'    => esc_html__('Title', 'nano'),
                        'value'      => '',
                        'param_name' => 'title',          
                        'description' => esc_html__( 'Enter Title.', 'nano' ),              
                    ),                
                    array(
                        "type"        => "textarea_html",
                        "class"       => "",
                        'heading'     => esc_html__( 'Description', 'nano' ),
                        "param_name"  => "description",
                        "value"       => '', 
                        'description' => esc_html__( 'Enter description.', 'nano' ),
                    ),
                    array(
                        'type'    => 'dropdown',
                        'heading' => esc_html__('Title align', 'nano'),
                        'value' => array(
                            esc_html__('Center', 'nano') => 'text-center',
                            esc_html__('Left', 'nano')   => 'text-left',
                            esc_html__('Right', 'nano')  => 'text-right',                           
                        ),
                        'param_name' => 'title_align',     
                        'std'        => 'left',                
                    ),
                    array(
                        'type'       => 'vc_link',
                        'heading'    => esc_html__( 'Button', 'nano' ),
                        'param_name' => 'url',
                    ),               
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' )
                    ),
                    array(
                        'type'       => 'css_editor',
                        'heading'    => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group'      => esc_html__( 'Design options', 'nano' ),
                    )
                )
            )
        );
    }
}