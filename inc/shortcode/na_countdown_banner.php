<?php

if (!function_exists('nano_shortcode_countdown_banner')) {
    function nano_shortcode_countdown_banner($atts){    
        $atts = shortcode_atts(array(
            'date'        => '2019/12/12',
            'title'       => '',            
            'align'       => 'text-left',
            'description' => '',
            'image_box'   => '',                        
            'url'         => '',            
            'el_class'    => '',
            'css'         => '',        
        ), $atts);
        ob_start();
        nano_template_part('shortcode', 'countdown-banner', array('atts' => $atts));
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_countdown_banner', 'nano_shortcode_countdown_banner');

add_action('vc_before_init', 'nano_countdown_banner_integrate_vc');
if (!function_exists('nano_countdown_banner_integrate_vc')) {
    function nano_countdown_banner_integrate_vc(){
        vc_map(array(
            'name'     => esc_html__('NA Countdown Timer', 'nano'),
            'base'     => 'na_countdown_banner',
            'category' => esc_html__('NA', 'nano'),
            'icon'     => 'nano-countdown-banner',
            'description' => esc_html__('Displays a banner with countdown timer', 'nano'),
            "params"   => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Date', 'nano' ),
                    'param_name'  => 'date',
                    'description' => esc_html__( 'Final date in the format Y/m/d. For example 2020/12/12', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),                
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__("Title", 'nano'),
                    "param_name"  => "title",
                    "admin_label" => true,
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),                
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__( 'Title Align', 'nano' ),
                    'param_name' => 'align',
                    'value'      => array(
                        esc_html__( 'Center', 'nano' ) => 'text-center',
                        esc_html__( 'Left', 'nano' )   => 'text-left',
                        esc_html__( 'Right', 'nano' )  => 'text-right'
                    ),
                    'std'   => 'text-left',
                    'group' => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    "type"        => "textarea_html",
                    "holder"      => "div",
                    "class"       => "",
                    'heading'     => esc_html__( 'Description', 'nano' ),
                    "param_name"  => "description",
                    "value"       => '',
                    'description' => esc_html__( 'Enter description for title.', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    "type"        => "attach_image",
                    "description" => esc_html__("Upload an image.", 'nano'),
                    "param_name"  => "image_box",
                    "value"       => '',
                    'heading'     => esc_html__('Countdown Banner', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    'type'       => 'vc_link',
                    'heading'    => esc_html__( 'Button', 'nano' ),
                    'param_name' => 'url',
                    'group'      => esc_html__( 'Settings', 'nano' ),
                ),                 
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'nano' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' ),
                    'group'       => esc_html__( 'Settings', 'nano' ),
                ),
                array(
                    'type'       => 'css_editor',
                    'heading'    => esc_html__( 'Css', 'nano' ),
                    'param_name' => 'css',
                    'group'      => esc_html__( 'Design options', 'nano' ),
                )
            )
        ));
    }
}
