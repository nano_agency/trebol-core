<?php

/* ================================================================================== */
/* Banners Split Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_banner_split')) {
    function na_shortcode_banner_split($atts, $content) {
        $atts = shortcode_atts(
            array(                                            
                'image_box_1'      => '',
                'title_1'          => '',
                'subtitle_1'       => '',           
                'url_1'            => '',           
                'image_box_2'      => '',
                'title_2'          => '',                         
                'url_2'            => '',      
                'position_layouts' => 'left',      
                'el_class'         => '',     
                'css'              => '',                         
            ), $atts);
        ob_start();            
            nano_template_part('shortcode', 'banner-split' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_banner_split', 'na_shortcode_banner_split');

add_action('vc_before_init', 'na_banner_split_integrate_vc');

if (!function_exists('na_banner_split_integrate_vc')) {
    function na_banner_split_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Banners Split Layout', 'nano'),
                'base' => 'na_banner_split',                
                'icon'     => 'nano-banner-split',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Create beautiful banner with split-divide layouts', 'nano'), 
                'params' => array(    
                    array(
                        "type"        => "attach_image",
                        'heading'     => esc_html__('Image', 'nano' ),
                        "description" => esc_html__("Upload a image.", 'nano'),
                        "param_name"  => "image_box_1",
                        "value"       => "",
                        'group'       => esc_html__( 'Banner Large', 'nano' ),
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__('Title','nano'),
                        "param_name"  => "title_1",
                        'admin_label' => true,
                        'group'       => esc_html__( 'Banner Large', 'nano' ),
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__('Sub Title','nano'),
                        "param_name"  => "subtitle_1",
                        'admin_label' => true,
                        'group'       => esc_html__( 'Banner Large', 'nano' ),
                    ),                            
                    array(
                        'type'       => 'vc_link',
                        'heading'    => esc_html__( 'Button', 'nano' ),
                        'param_name' => 'url_1',
                        'group'       => esc_html__( 'Banner Large', 'nano' ),
                    ),
                    array(
                        "type"        => "attach_image",
                        'heading'     => esc_html__('Image', 'nano' ),
                        "description" => esc_html__("Upload a image.", 'nano'),
                        "param_name"  => "image_box_2",
                        "value"       => "",
                        'group'       => esc_html__( 'Banner Small', 'nano' ),
                    ),
                    array(
                        "type"        => "textarea",
                        "class"       => "",
                        "heading"     => esc_html__('Title','nano'),
                        "param_name"  => "title_2",
                        'admin_label' => true,
                        'group'       => esc_html__( 'Banner Small', 'nano' ),
                    ),
                    array(
                        'type'       => 'vc_link',
                        'heading'    => esc_html__( 'Button', 'nano' ),
                        'param_name' => 'url_2',
                        'group'       => esc_html__( 'Banner Small', 'nano' ),
                    ),
                    array(
                        'type'    => 'dropdown',
                        'heading' => esc_html__('Position Layouts', 'nano'),
                        'value'   => array(
                            esc_html__('Left', 'nano')  => 'left',
                            esc_html__('Right', 'nano') => 'right',
                        ),
                        'std'        => 'left',                        
                        'param_name' => 'position_layouts', 
                        'group'      => esc_html__( 'Settings', 'nano' ),                       
                    ),     
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' ),   
                        'group'       => esc_html__( 'Settings', 'nano' ),                                            
                    ),              
                    array(
                        'type'       => 'css_editor',
                        'heading'    => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group'      => esc_html__( 'Design options', 'nano' ),
                    )
                )                 
            )        
        );
    }
}