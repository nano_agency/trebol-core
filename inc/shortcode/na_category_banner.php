<?php

/* ================================================================================== */
/* Category Banners Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_category_banners')) {
    function na_shortcode_category_banners($atts, $content) {
        $atts = shortcode_atts(
            array(                            
                'style'    => 'style1',
                'el_class' => '',
                'css'      => '',
                'items'    => '',
            ), $atts);

        ob_start();            
            nano_template_part('shortcode', 'category-banner' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_category_banners', 'na_shortcode_category_banners');

add_action('vc_before_init', 'na_category_banners_integrate_vc');

if (!function_exists('na_category_banners_integrate_vc')) {
    function na_category_banners_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Category Banners', 'nano'),
                'base' => 'na_category_banners',                
                'icon'     => 'nano-category-banners',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show a block Featured collections with Banners', 'nano'),       
                'params' => array( 
                    array(
                        "type"       => "dropdown",
                        "heading"    => esc_html__("Layout Style", 'nano'),
                        "param_name" => "style",
                        'value'      => array(
                            esc_html__('Layout Style 1', 'nano') => 'style1',
                            esc_html__('Layout Style 2', 'nano') => 'style2',                           
                        ),
                        'std'        => 'style1',
                        'group' => esc_html__( 'Settings', 'nano' ),
                    ), 
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' ),
                        'group' => esc_html__( 'Settings', 'nano' ),
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Category with Banners', 'nano' ),
                        'param_name' => 'items',
                        'group' => esc_html__( 'Profiles', 'nano' ),
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("Upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'   => esc_html__('Category Image', 'nano' ),
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title','nano'),
                                "param_name" => "title",
                                'admin_label' => true,
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Number Items','nano'),
                                "param_name" => "number",
                                'admin_label' => true,
                            ),
                            array(
                                'type' => 'vc_link',
                                'heading' => esc_html__( 'Button', 'nano' ),
                                'param_name' => 'url',
                            )                          
                        )
                    ),                     
                    array(
                        'type' => 'css_editor',
                        'heading' => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => esc_html__( 'Design options', 'nano' ),
                    )
                )
            )
        );
    }
}