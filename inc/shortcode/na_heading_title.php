<?php

if (!function_exists('nano_shortcode_heading_title')) {
    function nano_shortcode_heading_title($atts){    
        $atts = shortcode_atts(array(
            'title'       => '',
            'subtitle'    => '',
            'align'       => 'text-center',
            'description' => '',
            'style'       => 'style1',
            'url'         => '',
            'el_class'    => '',
            'css'         => ''
        ), $atts);
        ob_start();
        nano_template_part('shortcode', 'heading-title', array('atts' => $atts));
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_heading_title', 'nano_shortcode_heading_title');


add_action('vc_before_init', 'nano_heading_title_integrate_vc');
if (!function_exists('nano_heading_title_integrate_vc')) {
    function nano_heading_title_integrate_vc(){
        vc_map(array(
            'name'     => esc_html__('NA Heading Title', 'nano'),
            'base'     => 'na_heading_title',
            'category' => esc_html__('NA', 'nano'),
            'icon'     => 'nano-heading-title',
            "params"   => array(
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__("Title", 'nano'),
                    "param_name"  => "title",
                    "admin_label" => true
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__("Sub Title", 'nano'),
                    "param_name"  => "subtitle",
                    "admin_label" => true
                ),
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__( 'Title Align', 'nano' ),
                    'param_name' => 'align',
                    'value'      => array(
                        esc_html__( 'Center', 'nano' ) => 'text-center',
                        esc_html__( 'Left', 'nano' )   => 'text-left',
                        esc_html__( 'Right', 'nano' )  => 'text-right'
                    ),
                ),
                array(
                    "type"        => "textarea_html",
                    "holder"      => "div",
                    "class"       => "",
                    'heading'     => esc_html__( 'Description', 'nano' ),
                    "param_name"  => "description",
                    "value"       => '',
                    'description' => esc_html__( 'Enter description for title.', 'nano' )
                ),
                array(
                    "type"       => "dropdown",
                    "heading"    => esc_html__("Style", 'nano'),
                    "param_name" => "style",
                    'value'      => array(
                        esc_html__('Style 1 - Default - Dark Text', 'nano')  => 'style1',
                        esc_html__('Style 2 - Regular - Light Text', 'nano') => 'style2',
                        esc_html__('Style 3 - Medium - Dark Text', 'nano')   => 'style3',                        
                        esc_html__('Style 4 - Small - Dark Text', 'nano')    => 'style4',                        
                        esc_html__('Style 5 - Large - Light Text', 'nano')   => 'style5',                           
                    ),
                    'std'        => 'style1'
                ),  
                array(
                    'type'       => 'vc_link',
                    'heading'    => esc_html__( 'Button', 'nano' ),
                    'param_name' => 'url',
                ),               
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'nano' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' )
                ),
                array(
                    'type'       => 'css_editor',
                    'heading'    => esc_html__( 'Css', 'nano' ),
                    'param_name' => 'css',
                    'group'      => esc_html__( 'Design options', 'nano' ),
                )
            )
        ));
    }
}
