<?php
if (!function_exists('na_shortcode_masonry_category')) {
    function na_shortcode_masonry_category($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'css'      => '',
                'items'    => '',
                'style'    => 'style1',
                'el_class' => '',                
            ), $atts);
        ob_start();
            nano_template_part('shortcode', 'masonry-category' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_masonry_category', 'na_shortcode_masonry_category');

add_action('vc_before_init', 'na_masonry_category_integrate_vc');

if (!function_exists('na_masonry_category_integrate_vc')) {
    function na_masonry_category_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Masonry Categories', 'nano'),
                'base' => 'na_masonry_category',                
                'icon'     => 'nano-masonry-category',          
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Box with layout is masonry Category', 'nano'),
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Masonry Categories Layout Settings', 'nano' ),
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('Category Image', 'nano' ),
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__('Description','nano'),
                                "param_name" => "content_box",
                            ),
                            array(
                                'type' => 'vc_link',
                                'heading' => esc_html__( 'URL (Link)', 'nano' ),
                                'param_name' => 'link_box',
                                'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                            ),
                        ),
                    ),
                    array(
                        "type"       => "dropdown",
                        "heading"    => esc_html__("Layout Style", 'nano'),
                        "param_name" => "style",
                        'value'      => array(
                            esc_html__('Layout Style 1', 'nano') => 'style1',
                            esc_html__('Layout Style 2', 'nano') => 'style2',                                                      
                            esc_html__('Layout Style 3', 'nano') => 'style3',                                                      
                            esc_html__('Layout Style 4', 'nano') => 'style4',
                            esc_html__('Layout Style 5', 'nano') => 'style5',                                                                                                            
                        ),
                        'std'        => 'style1',                        
                    ), 
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' )
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => esc_html__( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => esc_html__( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}